<footer class="site-footer" id="contacts">
    <div class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="foot-about">
                        <h2><a href="#"><img class="logo-footer" src="{{ asset('landing/images/system/logo-expert.png') }}" alt=""></a></h2>

                        <p>Система проверки медицинских карт по законодательству. Все отчёты будут сданы без штрафов.</p>

                        <p class="copyright">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright ©<script>
                                document.write(new Date().getFullYear());
                            </script> Все права защищены
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div><!-- .foot-about -->
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0">
                    <div class="foot-contact">
                        <h2>Контакты</h2>

                        <ul class="p-0 m-0">
                            <!-- <li><span>Addtress:</span>Mitlton Str. 26-27 London UK</li> -->
                            <li><span>Телефон:</span>+7 (992) 003-99-63</li>
                            <li><span>Email:</span>it.manager@internet.ru</li>
                        </ul>
                    </div>
                </div><!-- .col -->

                <div class="col-12 col-md-6 col-lg-4 mt-5 mt-md-0">
                    <div class="foot-links">
                        <h2>Полезные ссылки</h2>

                        <ul class="p-0 m-0">
                            <li><a href="/">Главная</a></li>
                            <li><a href="#our-departments">Сервисы</a></li>
                            <!-- <li><a href="#">Departments</a></li> -->
                            <li><a href="#news">Новости</a></li>
                            <li><a href="#contacts">Контакты</a></li>
                        </ul>
                    </div><!-- .foot-links -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .footer-widgets -->
</footer><!-- .site-footer -->