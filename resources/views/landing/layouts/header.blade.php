<header class="site-header">
    <div class="nav-bar">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                    <div class="site-branding d-flex align-items-center">
                        <a class="d-block " href="/" rel="home"><img class="d-block logo" src="{{ asset('landing/images/system/logo-expert.png') }}" alt="logo"></a>
                    </div><!-- .site-branding -->

                    <nav class="site-navigation d-flex justify-content-end align-items-center">
                        <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-items-center">
                            <li class="current-menu-item"><a href="/">Главная</a></li>
                            <li><a href="#our-departments">Сервисы</a></li>
                            <li><a href="#news">Новости</a></li>
                            <!-- <li><a href="#about">О нас</a></li> -->
                            <li><a href="#contacts">Контакты</a></li>

                            <li class="call-btn button gradient-bg mt-3 mt-md-0">
                                <a class="d-flex justify-content-center align-items-center" href="{{ route('backoffice.desktop.index') }}">
                                    <!-- <img src="http://heart.local/landing/images/emergency-call.png">--> Войти
                               </a>
                            </li>
                        </ul>
                    </nav><!-- .site-navigation -->

                    <div class="hamburger-menu d-lg-none">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div><!-- .hamburger-menu -->
                </div><!-- .col -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .nav-bar -->

    <div class="swiper-container hero-slider" >
        <div class="swiper-wrapper" >
            <div class="swiper-slide hero-content-wrap" style="background-image: url('/landing/images/slider/1.jpeg')" data-swiper-autoplay="8000">
                <div class="hero-content-overlay position-absolute w-100 h-100">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                                <header class="entry-header">
                                    <h1>Медэксперт - правильные отчёты</h1>
                                </header><!-- .entry-header -->

                                <div class="entry-content mt-2">
                                    <h5 style="color: #828282">Мы разработали уникальный продукт,
                                        который на основе клинических рекомендаций позволяет создавать 
                                        правильные отчёты, 
                                        которые пройдут проверку и помогут избежать штрафов
                                    </h5>
                                </div><!-- .entry-content -->

                                <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                    <a href="{{ route('backoffice.desktop.index') }}" class="button gradient-bg">Подключить</a>
                                </footer>
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .hero-content-overlay -->
            </div><!-- .hero-content-wrap -->

            <div class="swiper-slide hero-content-wrap" style="background-image: url('/landing/images/slider/2.jpeg')" data-swiper-autoplay="8000">
                <div class="hero-content-overlay position-absolute w-100 h-100">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                                <header class="entry-header">
                                    <h1>Делаем сложное простым</h1>
                                </header><!-- .entry-header -->

                                <div class="entry-content mt-2">
                                    <h5 style="color: #828282">Мы разработали уникальный продукт,
                                        который на основе клинических рекомендаций позволяет создавать 
                                        правильные отчёты, которые пройдут проверку и помогут избежать штрафов.</h5>
                                </div><!-- .entry-content -->

                                <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                    <a href="{{ route('backoffice.desktop.index') }}" class="button gradient-bg">Подключить</a>
                                 </footer><!--.entry-footer -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .hero-content-overlay -->
            </div><!-- .hero-content-wrap -->

            <div class="swiper-slide hero-content-wrap" style="background-image: url('/landing/images/slider/3.jpeg')" data-swiper-autoplay="8000">
                <div class="hero-content-overlay position-absolute w-100 h-100">
                    <div class="container h-100">
                        <div class="row h-100">
                            <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                                <header class="entry-header">
                                    <h1>Создаём правильнве отчёты </h1>
                                </header><!-- .entry-header -->

                                <div class="entry-content mt-2">
                                    <h5 style="color: #828282">Мы разработали уникальный продукт,
                                        который на основе клинических рекомендаций позволяет создавать 
                                        правильные отчёты, которые пройдут проверку и помогут избежать штрафов.</h5>
                                </div><!-- .entry-content -->

                                <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                    <a href="{{ route('backoffice.desktop.index') }}" class="button gradient-bg">Подключить</a>
                            </footer> <!--.entry-footer -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                    </div><!-- .container -->
                </div><!-- .hero-content-overlay -->
            </div><!-- .hero-content-wrap -->
        </div><!-- .swiper-wrapper -->

        <div class="pagination-wrap position-absolute w-100">
            <div class="swiper-pagination d-flex flex-row flex-md-column"></div>
        </div><!-- .pagination-wrap -->
    </div><!-- .hero-slider -->
</header><!-- .site-header -->
<!-- <div class="homepage-boxes">