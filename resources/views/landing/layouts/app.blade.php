<html lang="en" style="scroll-behavior: auto;"><head>
    <title>Отчёты в медицине - интеллектуальная система</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('landing/css/bootstrap.min.css') }} ">

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('landing/css/swiper.min.css') }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('landing/style.css') }}">

</head>

<body>

@include('landing.layouts.header')
@yield('content')
@include('landing.layouts.footer')

<script type="text/javascript" src="{{ asset('landing/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.collapsible.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/swiper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.countdown.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/circle-progress.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.countTo.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/jquery.barfiller.js') }}"></script>
<script type="text/javascript" src="{{ asset('landing/js/custom.js') }}"></script>

</body></html>