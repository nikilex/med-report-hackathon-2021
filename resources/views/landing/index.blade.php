@extends('landing.layouts.app')

@section('content')
<!-- <div class="homepage-boxes">
     <div class="container">
         <div class="row">
             <div class="col-12 col-md-6 col-lg-4">
                 <div class="opening-hours">
                     <h2 class="d-flex align-items-center">Opening Hours</h2>

                     <ul class="p-0 m-0">
                         <li>Monday - Thursday <span>8.00 - 19.00</span></li>
                         <li>Friday <span>8.00 - 18.30</span></li>
                         <li>Saturday <span>9.30 - 17.00</span></li>
                         <li>Sunday <span>9.30 - 15.00</span></li>
                     </ul>
                 </div>
             </div>

             <div class="col-12 col-md-6 col-lg-3 mt-5 mt-md-0">
                 <div class="emergency-box">
                     <h2 class="d-flex align-items-center">Emergency</h2>

                     <div class="call-btn button gradient-bg">
                         <a class="d-flex justify-content-center align-items-center" href="#"><img src="{{ asset('landing/images/emergency-call.png') }}"> +34 586 778 8892</a>
                     </div>

                     <p>Lorem ipsum dolor sit amet, cons ectetur adipiscing elit. Donec males uada lorem maximus mauris sceler isque, at rutrum nulla.</p>
                 </div>
             </div>

             <div class="col-12 col-md-6 col-lg-5 mt-5 mt-lg-0">
                 <div class="appointment-box">
                     <h2 class="d-flex align-items-center">Make an Appointment</h2>

                     <form class="d-flex flex-wrap justify-content-between">
                         <select class="select-department">
                             <option value="value1">Select Department</option>
                             <option value="value2">Select Department 1</option>
                             <option value="value3">Select Department 2</option>
                         </select>

                         <select class="select-doctor">
                             <option>Select Doctor</option>
                             <option>Select Doctor 1</option>
                             <option>Select Doctor 2</option>
                         </select>

                         <input type="text" placeholder="Name">

                         <input type="number" placeholder="Phone No">

                         <input class="button gradient-bg" type="submit" value="Boom Appoitnment">
                     </form>
                 </div>

             </div>
         </div>
     </div>
 </div> -->

 <div class="our-departments" id="our-departments">
     <div class="container">
         <div class="row">
             <div class="col-12">
                 <div class="our-departments-wrap">
                     <h2>Сервисы</h2>

                     <div class="row">
                         <div class="col-12 col-md-6 col-lg-4">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap flex-column align-items-start">
                                     <img src="{{ asset('landing/images/cardiogram.png') }}" alt="">
                                     <br>
                                     <h3>Медэксперт - автоматизация экспертих</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Новые технологии позволяют нам рутинную работу врачей
                                         поручить программному обеспечению. Он быстрее и точнее.</p>
                                 </div>

                                 <!-- <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer> -->
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap flex-column align-items-start">
                                     <img src="{{ asset('landing/images/icons/recomendations.png') }}" alt="">
                                     <br>
                                     <h3>Рекомендации врачам</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Система анализа данных напрямую работает с перечнем рекомендаций врачам.
                                         То что врач будет искать по справочнику, наша система автоматически
                                         находит и предлагает на рассмотрение</p>
                                 </div>

                                 <!-- <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer> -->
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap flex-column align-items-start">
                                     <img src="{{ asset('landing/images/blood-sample-2.png') }}" alt="">
                                     <br>
                                     <h3>Анализ данных</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Система постоянно анализирует базы данных министерств для своевременного обновления
                                     отчётов </p>
                                 </div>

                                 <!-- <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer> -->
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap flex-column align-items-start">
                                     <img src="{{ asset('landing/images/glasses.png') }}" alt="">
                                     <!-- <img src="{{ asset('landing/images/teeth.png') }}" alt=""> -->
                                     <br>
                                     <h3>Более 2500 отчётов создано</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Каждый день более добавляются множество новых отчтов, 
                                     которые прошли проверку без затруднений.</p>
                                 </div>

                                 <!-- <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer> -->
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap flex-column align-items-start">
                                     <img src="{{ asset('landing/images/icons/profilact.png') }}" alt="">
                                     <br>
                                     <h3>Профилактика</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Система позволяет провести промежуточные отчэты и отправить их на электронную почту,
                                     а ещё сделает напоминание о предстоящей проверке</p>
                                 </div>

                                 <!-- <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer> -->
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap flex-column align-items-start">
                                     <img src="{{ asset('landing/images/scanner.png') }}" alt="">
                                     <br>
                                     <h3>Самостоятельные исследования</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Мы постоянно проводим исследования новых документов и внедряем дополнительный функционал
                                     в нашу систему для своевременной сдачи отчэтности</p>
                                 </div>

                                 <!-- <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer> -->
                             </div>
                         </div>

                         <!-- <div class="col-12 col-md-6 col-lg-4 mb-md-0">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap align-items-center">
                                     <img src="{{ asset('landing/images/bones.png') }}" alt="">

                                     <h3>Orthopaedy</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus.</p>
                                 </div>

                                 <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer>
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4 mb-lg-0">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap align-items-center">
                                     <img src="{{ asset('landing/images/blood-donation-2.png') }}" alt="">

                                     <h3>Pediatry</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien.</p>
                                 </div>

                                 <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer>
                             </div>
                         </div>

                         <div class="col-12 col-md-6 col-lg-4 mb-0">
                             <div class="our-departments-cont">
                                 <header class="entry-header d-flex flex-wrap align-items-center">
                                     <img src="{{ asset('landing/images/glasses.png') }}" alt="">

                                     <h3>Ophthalmology</h3>
                                 </header>

                                 <div class="entry-content">
                                     <p>Lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus.</p>
                                 </div>

                                 <footer class="entry-footer">
                                     <a href="#">read more</a>
                                 </footer>
                             </div>
                         </div> -->
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>

 <!-- <section class="testimonial-section">
     <div class="container">
         <div class="row">
             <div class="col-12">
                 <h2>Pacient’s Testimonials</h2>
             </div>
         </div>
     </div>

    
     <div class="testimonial-slider">
         <div class="container">
             <div class="row">
                 <div class="col-12 col-lg-9">
                     <div class="testimonial-bg-shape">
                         <div class="swiper-container testimonial-slider-wrap">
                             <div class="swiper-wrapper">
                                 <div class="swiper-slide">
                                     <div class="entry-content">
                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Curabitur ut augue finibus, luctus tortor at, ornare erat. Nulla facilisi. Sed est risus, laoreet et quam non, viverra accumsan leo.</p>
                                     </div>

                                     <div class="entry-footer">
                                         <figure class="user-avatar">
                                             <img src="{{ asset('landing/images/user-1.jpg') }}" alt="">
                                         </figure>

                                         <h3 class="testimonial-user">Russell Stephens <span>University in UK</span></h3>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="entry-content">
                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Curabitur ut augue finibus, luctus tortor at, ornare erat. Nulla facilisi. Sed est risus, laoreet et quam non, viverra accumsan leo.</p>
                                     </div>

                                     <div class="entry-footer">
                                         <figure class="user-avatar">
                                             <img src="{{ asset('landing/images/user-2.jpg') }}" alt="">
                                         </figure>

                                         <h3 class="testimonial-user">Russell Stephens <span>University in UK</span></h3>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="entry-content">
                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Curabitur ut augue finibus, luctus tortor at, ornare erat. Nulla facilisi. Sed est risus, laoreet et quam non, viverra accumsan leo.</p>
                                     </div>

                                     <div class="entry-footer">
                                         <figure class="user-avatar">
                                             <img src="{{ asset('landing/images/user-3.jpg') }}" alt="">
                                         </figure>

                                         <h3 class="testimonial-user">Russell Stephens <span>University in UK</span></h3>
                                     </div>
                                 </div>

                                 <div class="swiper-slide">
                                     <div class="entry-content">
                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus. Curabitur ut augue finibus, luctus tortor at, ornare erat. Nulla facilisi. Sed est risus, laoreet et quam non, viverra accumsan leo.</p>
                                     </div>

                                     <div class="entry-footer">
                                         <figure class="user-avatar">
                                             <img src="{{ asset('landing/images/user-4.jpg') }}" alt="">
                                         </figure>

                                         <h3 class="testimonial-user">Russell Stephens <span>University in UK</span></h3>
                                     </div>
                                 </div>
                             </div>

                             <div class="swiper-pagination-wrap">
                                 <div class="container">
                                     <div class="row">
                                         <div class="col-12">
                                             <div class="swiper-pagination position-relative flex justify-content-center align-items-center"></div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section> -->

 <div class="the-news" id="news">
     <div class="container">
         <div class="row">
             <div class="col-12">
                 <h2>Новости</h2>

                 <div class="row">
                     <div class="col-12 col-md-6 col-lg-4">
                         <div class="the-news-wrap">
                             <figure class="post-thumbnail">
                                 <a href="#"><img src="{{ asset('landing/images/news/1/accel.jpeg') }}" alt=""></a>
                             </figure>

                             <header class="entry-header">
                                 <h3>Акселератор</h3>

                                 <div class="post-metas d-flex flex-wrap align-items-center">
                                     <div class="posted-date"><label>Дата: </label><a href="#">Март 31, 2021</a></div>

                                     <div class="posted-by"><label>Автор: </label><a href="#">Егор Богданов</a></div>

                                     <!-- <div class="post-comments"><a href="#">2 Comments</a></div> -->
                                 </div>
                             </header>

                             <div class="entry-content">
                                 <p>Сегодня, наш проект в системе здравоохранения, прошел жёсткий отбор в
                                     акселератор Цифрового Прорыва￼. Выбирали среди других победителей этого конкурса.
                                     Конкуренция, 15 команд на место – это очень много.
                                     Но мы справились, значит, у нашего решения будет будущее￼!
                                     #цифровойпрорыв #цифроваятрансформация #ai #ml #bigdata #datascience</p>
                             </div>
                         </div>
                     </div>

                     <div class="col-12 col-md-6 col-lg-4">
                         <div class="the-news-wrap">
                             <figure class="post-thumbnail">
                                 <a href="#"><img src="{{ asset('landing/images/news/2/pitch.jpeg') }}" alt=""></a>
                             </figure>

                             <header class="entry-header">
                                 <h3>Выступление на гранд-финале конкурса Цифровой Прорыв</h3>

                                 <div class="post-metas d-flex flex-wrap align-items-center">
                                     <div class="posted-date"><label>Дата: </label><a href="#">Декабрь 09, 2020</a></div>

                                     <div class="posted-by"><label>Автор: </label><a href="#">Егор Богданов</a></div>

                                     <!-- <div class="post-comments"><a href="#">2 Comments</a></div> -->
                                 </div>
                             </header>

                             <div class="entry-content">
                                 <p>Выступление нашего спикера на гранд-финале конкурса Цифровой Прорыв 2020.
                                     После выступления проект получил высокую оценку слушателей в число которых вошли
                                     Поляков С.И. директор фонда содействия инновациям (ФСИ),
                                     заместитель председателя правительства РФ Дмитрий Чернышенко,
                                     первый заместитель руководителя Администрации Президента РФ Сергей Кириенко</p>
                             </div>
                         </div>
                     </div>

                     <div class="col-12 col-md-6 col-lg-4">
                         <div class="the-news-wrap">
                             <figure class="post-thumbnail">
                                 <a href="#"><img src="{{ asset('landing/images/news/3/pobeda.jpeg') }}" alt=""></a>
                             </figure>

                             <header class="entry-header">
                                 <h3>Победа на Цифровом Прорыве 2020</h3>

                                 <div class="post-metas d-flex flex-wrap align-items-center">
                                     <div class="posted-date"><label>Дата: </label><a href="#">Декабрь 10, 2020</a></div>

                                     <div class="posted-by"><label>Автор: </label><a href="#">Алексей Никитин</a></div>

                                     <!-- <div class="post-comments"><a href="#">2 Comments</a></div> -->
                                 </div>
                             </header>

                             <div class="entry-content">
                                 <p>Команда IT-разработчиков «Форвард», в состав которой неизменно входят
                                     карпинский программист Алексей Никитин, дизайнер Иван Обухов и менеджер
                                     Егор Богданов из Нижнего Тагила, успешно пройдя все предварительные этапы,
                                     стала победительницей финала масштабного онлайн-хакатона «Цифровой прорыв». </p>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>

 <div class="subscribe-banner">
     <div class="container">
         <div class="row">
             <div class="col-12 col-lg-8 offset-lg-2">
                 <h2>Подписывайтесь на нашу новостную рассылку</h2>

                 <form>
                     <input type="email" placeholder="E-mail адрес">
                     <input class="button gradient-bg" type="submit" value="Подписаться">
                 </form>
             </div>
         </div>
     </div>
 </div>
@endsection