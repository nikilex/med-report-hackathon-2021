<div class="mt-3">
    @foreach($criteria as $value)
        <div class="custom-control custom-checkbox mb-2">
            <input type="checkbox" class="custom-control-input" id="{{ $value->id }}" name="criteria[]" value="{{ $value->id }}" checked>
            <label class="custom-control-label" for="{{ $value->id }}">{{ $value->name }}</label>
        </div>
    @endforeach
</div>