@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Методы API</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Методы API</h4>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4>Метод expert()</h4>
                <p>Метод POST</p>
                <p>URL: /api/expert</p>
                <p>Принимаемые значения:</p>
                <pre>
                    age: 25      - Возраст
                    diagnos: J01 - Код диагноза (коды по МКБ-10)
                    separator: , - Символ, который должен обозначать разделитель между строками обследования и лечения
                    criteria: 'Критерий 1, Критерий 2' - Тип: строка. Критерии качества
                    examination: 'Обследование 1, Обследование 2' - Тип: строка. Назначенные обследования
                    treatment: 'Лечение 1, Лечение 2' - Тип: строка. Назначенные лечения.
                </pre>
                
                <p>Возвращаемое значение</p>
                <pre>
                    {
                        "count_error_criteria": 7,    - Количество ошибок по критериям качества
                        "count_error_examination": 9, - Количество ошибок по назначениям обследования
                        "count_error_treatment": 28,  - Количество ошибок по назначениям лечения
                        "error_criteria": [           - Массив критериев, которые небыли назначены
                            {
                                "id": 1,
                                "name": "Выполнена рентгенография придаточных пазух носа или компьютерная томография придаточных пазух носа в случае отсутствия проведения данных исследований на догоспитальном этапе",
                                "diagnosis_id": 1,
                                "created_at": "2021-06-18T20:38:08.000000Z",
                                "updated_at": "2021-06-18T20:38:08.000000Z"
                            },
                            ...
                        ],

                        ---- Строка через сепаратор не назначеных обследований ------
                        "error_examination": "Бактериологическое исследование смывов и/или пунктатов из околоносовых полостей на аэробные и факультативно- анаэробные микроорганизмы,",
                        

                        ---- Строка через сепаратор не назначеных лечений ------
                        "error_treatment": "Лизаты бактерий,Кеторолак,Ибупрофен,Парацетамол,Парацетамол + Фенилэфрин + Фенирамин,Парацетамол + Фенилэфрин + Хлорфенамин,Парацетамол + Хлорфенамин + [Аскорбиновая кислота],Ксилометазолин,Нафазолин,Фенилэфрин,Ацетилцистеин + Туаминогептан,Диметинден + Фенилэфрин,Дексаметазон + Неомицин + Полимиксин B + Фенилэфрин,Морская вода,Натрия хлорид,Фрамицетин,Фузафунгин,Монтелукаст,Фенспирид,Миртол,Ацетилцистеин,Карбоцистеин,Цетиризин,Дезлоратадин,Лоратадин,Левоцетиризин,Фексофенадин,Эбастин",
                        
                        "updated_at": "2021-06-18T22:43:55.000000Z",
                        "created_at": "2021-06-18T22:43:55.000000Z",

                        
                        "id": 26  - Идентификатор обследования
                    }
                    </pre>
                
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection

@section('script-bottom')
@endsection