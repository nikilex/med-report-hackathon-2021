@extends('backoffice.layouts.vertical')


@section('css')
        <!-- Plugins css -->
        <link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Экспертиза карт</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Экспертиза карт</h4>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h4 class="header-title mt-0 mb-2">Экспертиза карты 1С в автоматическом режиме</h4>
                        <a class="mt-4" href="{{ route('backoffice.expertise.download-file') }}">Скачать тестоваую карту для загрузки</a>
                        <!-- <p>
                            В данном отчёте принимаются медицинские карточки выгруженные из 1С с форматом html
                        </p> -->
                        @error('htmlFile')
                        <div class="alert alert-danger "> {{ $message }}</div>
                        @enderror
                        @if(session('error'))
                        <div class="alert alert-danger"> {{ session('error') }}</div>
                        @endif
                        <form class="mt-4" action="{{ route('backoffice.expertise.parcer') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type='file' name="htmlFile" id='fileinput'>
                            <input type='submit' id='btnLoad' value='Отправить'>
                        </form>

                        <div class="row mt-5">
                            @if(isset($card) && $card)
                            <div class="col-12">
                            <!-- <h5>Карта заполнена на: 30%</h5> -->
                            </div>
                            <div class="col">
                                <h3 class="header-title mt-0 mb-2">Ваша карта</h3>
                                <p>Диагноз: {{ $card['diagnoses']['code'] }} {{ $card['diagnoses']['text'] }}</p>
                                <br>
                                <div class="examinations">
                                    <p>Обследование:</p>
                                    @foreach($card['examination'] as $examination)
                                    <p>{{ $examination }}</p>
                                    @endforeach
                                    <br>
                                    <p>Лечение:</p>
                                    @foreach($card['treatment'] as $treatment)
                                    <p>{{ $treatment }}</p>
                                    @endforeach
                                </div>
                            </div>

                            @endif

                            @if(isset($checkedCard) && $checkedCard)
                            <div class="col">
                                <h3 class="header-title mt-0 mb-2">Результат проверки</h3>
                                <p>Диагноз: {{ $checkedCard['diagnoses']['code'] }} {{ $card['diagnoses']['text'] }}</p>
                                <br>
                                <div class="examinations">
                                    <p>Обследование:</p>
                                    @foreach($checkedCard['examinations'] as $examination)
                                    @if($examination['type'] == 'examination')
                                    <p style="color: {{ $examination['color'] }}">{{ $examination['value'] }} - {{ $examination['message'] }}</p>
                                    @endif
                                    @endforeach
                                    <br>
                                    <p>Лечение:</p>
                                    @foreach($checkedCard['examinations'] as $examination)
                                    @if($examination['type'] == 'treatment')
                                    <p style="color: {{ $examination['color'] }}">{{ $examination['value'] }} - {{ $examination['message'] }}</p>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('script')

@endsection

@section('script-bottom')
        <!-- Plugins Js -->
        <script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
        <script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
        <script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
        <script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
        
        <!-- Init js-->
        <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

        <script>
            $('#diagnos').on('change', function () {
                let diagnos_id = $('#diagnos option:selected').val();
                let container = $('#criteria');
                let criteriaSpinner = $('#criteriaSpinner');

                criteriaSpinner.removeClass('d-none');
                container.html('');

                if(diagnos_id != '') {
                    $.post('/backoffice/getCriteria', {
                        '_token': '{{ csrf_token() }}',
                        diagnos_id: diagnos_id
                    }, function (res) {
                        criteriaSpinner.addClass('d-none');
                        container.html(res);
                    })
                }
            })
        </script>
@endsection