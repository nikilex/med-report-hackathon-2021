@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Мои заявки</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Мои заявки</h4>
    </div>
</div>
@endsection

@section('content')
@if (session('success'))
<div class="row">
    <div class="col">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>
                    На этой странице можно посмотреть информацию о своих заявках.
                </p>
                <a href="{{ route('backoffice.application.add') }}" class="btn btn-outline-primary mb-3">Добавить заявку</a>
                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Номер заявки</th>
                            <th>Описание события</th>
                            <th>Статус события</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach($applications as $application)
                        <tr>
                            <td> <a href="{{ route('backoffice.application.edit', ['id' => $application->id]) }}">{{ $application->id }}</a> </td>
                            <td>{{ $application->indastry()->first()->name }}</td>
                            <td>  <span style="color: #c1c11f">{{ $application->status == 1 ? 'На рассмотрении' : ''  }}</span>
                            <span style="color: green">{{ $application->status == 2 ? 'Принята' : ''  }}</span>
                            <span style="color: gray"> {{ $application->status == 3 ? 'На доработке' : ''  }}</span>
                            <span style="color: red">   {{ $application->status == 4 ? 'Отклонена' : ''  }}</span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')

@endsection

@section('script-bottom')
@endsection