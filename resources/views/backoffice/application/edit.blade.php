@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Создание заявки</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Создание заявки</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>
                    Заполните все обязательные поля и прикрепите файлы.
                </p>
                <form class="form-horizontal" method="POST" action="{{ route('backoffice.application.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Выберете конкурс</label>
                                <div class="col-lg-10">
                                    <select class="form-control custom-select @error('indastry_id') is-invalid @enderror" name="indastry">
                                        @foreach($indastries as $indastry)
                                        <option value="{{ $indastry->id }}" {{ $indastry->id == old('indastry_id') ? 'selected' : '' }}>{{ $indastry->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('indastry_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Система налогооблажения</label>
                                <div class="col-lg-10">
                                    <select class="form-control custom-select @error('taxation_system_id') is-invalid @enderror" name="taxation_system_id">
                                        @foreach($taxationSystems as $taxationSystem)
                                        <option value="{{ $taxationSystem->id }}" {{ $taxationSystem->id == old('taxation_system_id') ? 'selected' : '' }}>{{ $taxationSystem->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('taxation_system_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="number_employees">Количество сотрудников</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('number_employees') is-invalid @enderror" id="number_employees" name="number_employees" value="{{ old('number_employees') }}">
                                    @error('number_employees')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="number_branches">Количество точек общественного питания</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('number_branches') is-invalid @enderror" id="number_branches" name="number_branches" value="{{ old('number_branches') }}">
                                    @error('number_branches')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Сколько работаете в сфере общественного питания</label>
                                <div class="col-lg-10">
                                    <select class="form-control custom-select @error('experience') is-invalid @enderror" name="experience">
                                        <option value="менее 1 года">Менее 1 года</option>
                                        <option value="от 1 до 3 лет">От 1 до 3 лет</option>
                                        <option value="более 3 лет">Более 3 лет</option>
                                    </select>
                                    @error('experience')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Ваши клиенты</label>
                                <div class="col-lg-10">
                                    <select class="form-control custom-select @error('client_id') is-invalid @enderror" name="client_id">
                                        @foreach($clients as $client)
                                        <option value="{{ $client->id }}" {{ $client->id == old('client_id') ? 'selected' : '' }}>{{ $client->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('client_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="companies">ИНН компаний с которыми работаете более 3 мес.</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('companies') is-invalid @enderror" id="companies" name="companies[]" value="{{ old('companies') }}">
                                    @error('companies')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="mt-5">
                                <h4 class="header-title">Прикрепите файлы</h4>
                                <p class="sub-header">
                                    Поддерживаемые форматы word, pdf
                                </p>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="commercial_proposal_file">Коммерческое предложение</label>
                                <div class="col-lg-10">
                                    <input type="file" class="form-control" name="commercial_proposal_file" id="commercial_proposal_file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="menu_file">Меню с выходом блюд</label>
                                <div class="col-lg-10">
                                    <input type="file" class="form-control" name="menu_file" id="menu_file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="assortiment_menu_file">Ассортимент меню</label>
                                <div class="col-lg-10">
                                    <input type="file" class="form-control" name="assortiment_menu_file" id="assortiment_menu_file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="photo_set_lunch">Фото комплексного обеда (внешний вид, состав, упаковка)</label>
                                <div class="col-lg-10">
                                    <input type="file" class="form-control" name="photo_set_lunch" id="photo_set_lunch">
                                </div>
                            </div>
                            <div class="form-group mt-4">
                                <button type="submit" class="btn btn-outline-primary mb-3">Подать заявку</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')

@endsection

@section('script-bottom')
@endsection