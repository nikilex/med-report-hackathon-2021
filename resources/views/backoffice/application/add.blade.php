@extends('backoffice.layouts.vertical')


@section('css')
<style>
    .error {
        color: red;
    }
</style>
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Создание заявки</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Создание заявки</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>
                    Заполните все обязательные поля и прикрепите файлы.
                </p>
                <form class="form-horizontal" id="formApp" method="POST" action="{{ route('backoffice.application.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="step1-tab">
                            <form id="step-1-form" action="">
                                <h2>Шаг 1</h2>
                                <div class="row">
                                    <div class="col-9">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label">Выберете конкурс</label>
                                            <div class="col-lg-10">
                                                <select class="form-control custom-select @error('indastry_id') is-invalid @enderror" id="indastry_id" name="indastry_id">
                                                    <option value="">Выберите</option>
                                                    @foreach($indastries as $indastry)
                                                    <option value="{{ $indastry->id }}" {{ $indastry->id == old('indastry_id') ? 'selected' : '' }}>{{ $indastry->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('indastry_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="button" id="step1-button" class="btn btn-outline-primary mb-3 next">Далее</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="step2" role="tabpanel" aria-labelledby="step2-tab">
                            <h2>Шаг 2</h2>
                            <div class="row">
                                <div class="col-9">
                                    <h4 class="header-title">Прикрепите файлы</h4>
                                    <p class="sub-header">
                                        Поддерживаемые форматы word, pdf, jpg, jpeg, png<br>
                                        Максимальный размер файла 10МБ
                                    </p>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="commercial_proposal_file">Коммерческое предложение</label>
                                        <div class="col-lg-10">
                                            <input autocomplete="off" type="file" class="form-control @error('commercial_proposal_file') is-invalid @enderror" name="commercial_proposal_file" id="commercial_proposal_file" accept="image/jpeg,image/png,image/gif,image/jpg,application/pdf,application/doc,application/docx">
                                            @error('commercial_proposal_file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="menu_file">Меню с выходом блюд</label>
                                        <div class="col-lg-10">
                                            <input autocomplete="off" type="file" class="form-control @error('menu_file') is-invalid @enderror" name="menu_file" id="menu_file" accept="">
                                            @error('menu_file')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="photo_set_lunch">Фото комплексного обеда (внешний вид, состав, упаковка)</label>
                                        <div class="col-lg-10">
                                            <input autocomplete="off" type="file" class="form-control @error('photo_set_lunch') is-invalid @enderror" name="photo_set_lunch" id="photo_set_lunch" accept="image/jpeg,image/png,image/gif,image/jpg,application/pdf,application/doc,application/docx">
                                            @error('photo_set_lunch')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="button" id="step2-button" class="btn btn-outline-primary mb-3 next">Далее</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="step3" role="tabpanel" aria-labelledby="step3-tab">
                            <h2>Шаг 3</h2>
                            <div class="row">
                                <div class="col-9">
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Ваша система налогооблажения</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select @error('taxation_system_id') is-invalid @enderror" name="taxation_system_id">
                                                <option value="">Выбрать</option>
                                                @foreach($taxationSystems as $taxationSystem)
                                                <option value="{{ $taxationSystem->id }}" {{ $taxationSystem->id == old('taxation_system_id') ? 'selected' : '' }}>{{ $taxationSystem->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('taxation_system_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="number_employees">Количество ваших сотрудников</label>
                                        <div class="col-lg-10">
                                            <input autocomplete="off" type="text" class="form-control @error('number_employees') is-invalid @enderror" id="number_employees" name="number_employees" value="{{ old('number_employees') }}">
                                            @error('number_employees')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label" for="number_branches">Количество точек общественного питания</label>
                                        <div class="col-lg-10">
                                            <input autocomplete="off" type="text" class="form-control @error('number_branches') is-invalid @enderror" id="number_branches" name="number_branches" value="{{ old('number_branches') }}">
                                            @error('number_branches')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Сколько работаете в сфере общественного питания</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select @error('experience') is-invalid @enderror" id="experience" name="experience">
                                                <option value="">Выбрать</option>
                                                <option value="менее 1 года">Менее 1 года</option>
                                                <option value="от 1 до 3 лет">От 1 до 3 лет</option>
                                                <option value="более 3 лет">Более 3 лет</option>
                                            </select>
                                            @error('experience')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-4 col-form-label">Ваши клиенты</label>
                                        <div class="col-lg-10">
                                            @foreach($clients as $client)
                                            <div class="custom-control custom-checkbox">
                                                <input autocomplete="off" type="checkbox" class="custom-control-input" id="client{{ $client->id }}" name="client_id[]" value="{{ $client->id }}">
                                                <label class="custom-control-label" for="client{{ $client->id }}">{{ $client->name }}</label>
                                            </div>
                                            @endforeach

                                            @error('client_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="button" class="btn btn-outline-primary mb-3 next">Далее</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="step4" role="tabpanel" aria-labelledby="step3-tab">
                            <h2>Шаг 4</h2>
                            <div class="row">
                                <div class="col-9">
                                    <div class="form-group row companies">
                                        <label class="col-lg-4 col-form-label" for="companies">ИНН компаний с которыми работаете более 3 мес.</label>
                                        <div class="w-100"></div>
                                        <label class="col-lg-4 col-form-label">ИНН</label>
                                        <div class="col-lg-10 step">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col">
                                                            <input autocomplete="off" type="text" class="form-control @error('companies') is-invalid @enderror inn" name="companies[]" data-toggle="dropdown">
                                                            <div class="dropdown-menu dropdown-inn">
                                                                <p class="decription ml-2">Выберите вариант или начните ввод</p>
                                                                <div class="dadata">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label class="" for="name">Название компании</label>
                                                    <input autocomplete="off" type="text" class="form-control @error('name') is-invalid @enderror name" name="name[]">
                                                    <label class="" for="name">Адрес компании</label>
                                                    <input autocomplete="off" type="text" class="form-control @error('address') is-invalid @enderror address" name="address[]">
                                                    @error('companies')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <label class="col-lg-4 col-form-label mt-3">ИНН</label>
                                        <div class="col-lg-10 step ">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col">
                                                            <input autocomplete="off" type="text" class="form-control @error('companies') is-invalid @enderror inn" name="companies[]" data-toggle="dropdown">
                                                            <div class="dropdown-menu dropdown-inn">
                                                                <p class="decription ml-2">Выберите вариант или начните ввод</p>
                                                                <div class="dadata">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label class="" for="name">Название компании</label>
                                                    <input autocomplete="off" type="text" class="form-control @error('name') is-invalid @enderror name" name="name[]">
                                                    <label class="" for="name">Адрес компании</label>
                                                    <input autocomplete="off" type="text" class="form-control @error('address') is-invalid @enderror address" name="address[]">
                                                    @error('companies')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <label class="col-lg-4 col-form-label mt-3">ИНН</label>
                                        <div class="col-lg-10 step">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col">
                                                            <input autocomplete="off" type="text" class="form-control @error('companies') is-invalid @enderror inn" name="companies[]" data-toggle="dropdown">
                                                            <div class="dropdown-menu dropdown-inn">
                                                                <p class="decription ml-2">Выберите вариант или начните ввод</p>
                                                                <div class="dadata">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label class="" for="name">Название компании</label>
                                                    <input autocomplete="off" type="text" class="form-control @error('name') is-invalid @enderror name" name="name[]">
                                                    <label class="" for="name">Адрес компании</label>
                                                    <input autocomplete="off" type="text" class="form-control @error('address') is-invalid @enderror address" name="address[]">
                                                    @error('companies')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>



                                        <div class="col-10 buttons mt-3">
                                            <button type="button" class="btn btn-outline-info btn-block addCompany">+ Добавить компанию</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group mt-4">
                                        <button type="submit" class="btn btn-outline-primary mb-3">Подать заявку</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/localization/messages_ru.js') }}"></script>
<script src="{{ asset('js/validate.init.js') }}"></script>
<script src="{{ asset('js/dadata.init.js') }}"></script>
@endsection

@section('script-bottom')

@endsection