@extends('backoffice.layouts.vertical')


@section('css')
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="{{ route('backoffice.journal.index') }}">Журнал</a></li>
                <li class="breadcrumb-item active" aria-current="page">Экспертиза</li>
            </ol>
        </nav>
        <div class="row p-0">
            <div class="col-auto">
                <a href="{{ route('backoffice.journal.index') }}">
                    <i data-feather="arrow-left"></i>
                </a>
            </div>
            <div class="col-auto">
                <h4 class="mb-1 mt-0">Экспертиза</h4>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h1>Результат экспертизы {{ isset($expert['id']) ? '№ '.$expert['id'] : '' }}</h1>
                <form action="{{ route('backoffice.export') }}" method="POST">
                    @csrf
                    <input type="hidden" name="export_data" id="input_export_data">
                    <div class="row">
                        <div class="col text-right">
                            <button type="button" class="btn btn-info " id="export">Выгрузить печатный протокол</button>
                        </div>
                    </div>
                </form>
                <div id="export_data">
                    <h4 class="mt-4">Выявленные нарушения по критериям:</h4>
                    <p>Отсутствуют пункты:</p>
                    @foreach($criterias as $criteria)
                    <p style="color:red">{{ $criteria->name }}</p>
                    @endforeach
                    <h4 class="mt-4">Выявленные нарушения по обследованию:</h4>
                    <p>Отсутствуют пункты:</p>
                    @foreach($expert['examinations'] as $examination)
                    <p style="color:red">{{ $examination }}</p>
                    @endforeach
                    <h4 class="mt-4">Выявленные нарушения по лечению:</h4>
                    <p>Отсутствуют пункты:</p>
                    @foreach($expert['treatment'] as $treatment)
                    <p style="color:red">{{ $treatment }}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection

@section('script-bottom')
<script>
    $('#export').on('click', function() {
        var export_data = $('#export_data');

        var input_export_data = $('#input_export_data');

        input_export_data.val(export_data.html());

        this.closest('form').submit();
        // $.post('/backoffice/export', {
        //     export_data: export_data.html()
        // }, function (res) {

        // })
    })
</script>
@endsection