@extends('backoffice.layouts.vertical')


@section('css')
        <!-- Plugins css -->
        <link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<style>
    .second-text {
        font-size: 11px;
    }
</style>
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Экспертиза для эксперта</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Экспертиза для эксперта в ручном режиме</h4>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-1">Экспертиза карты в ручном режиме для эксперта</h4>
                <span class="second-text">Данные карты могут подгружаются из системы МИС</span>
                 <div class="row mt-4">
                    <div class="col">
                        <form class="form-horizontal" action="{{ route('backoffice.expertise.expert') }}" method="POST">
                            @csrf
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="age">Возраст</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="age" name="age" value="25">
                                </div>
                            </div>

                            <div class="form-group mt-3 mt-sm-0 row">
                                <label class="col-lg-2 col-form-label" >Диагноз</label>
                                <div class="col-lg-10">
                                    <select data-plugin="customselect" class="form-control" id="diagnos" name="diagnos">
                                        <option value='' selected>Выберите диагноз</option>
                                        @foreach($diagnosis as $diagnos)
                                            <option value="{{ $diagnos->id }}" {{ $diagnos->id == 1 ? 'selected' : '' }}>{{ $diagnos->code }} {{ $diagnos->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="criteria">Критерии качества <br><span class="second-text">Критерии подгружаются из Приказа Минздрава России от 10.05.2017 N 203н.<br> Необходимо отключить те, которые небыли назначены</span> </label>

                                <div class="spinner-border text-primary m-2 d-none" id="criteriaSpinner" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                                <div class="col-lg-10" id="criteria">
                                   Выберите диагноз
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="examination">Обследование <br><span class="second-text">Данные могут подгружаются из системы МИС</span></label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" rows="5" id="examination" name="examination">Серологические исследования на вирусы респираторных инфекций, Бактериологическое исследование смывов и/или пунктатов из околоносовых полостей на аэробные и факультативно- анаэробные микроорганизмы, Определение чувствительности микроорганизмов к антибиотикам и другим лекарственным препаратам</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="treatment">Лечение<br><span class="second-text">Данные могут подгружаются из системы МИС</span></label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" rows="5" id="treatment" name="treatment">Амоксициллин+ [Клавулановая кислота], Цефуроксим</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-info">Провести экспертизу</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection

@section('script-bottom')
        <!-- Plugins Js -->
        <script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
        <script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
        <script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
        <script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
        
        <!-- Init js-->
        <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

        <script>
            $('#diagnos').on('change', function () {
                let diagnos_id = $('#diagnos option:selected').val();
                let container = $('#criteria');
                let criteriaSpinner = $('#criteriaSpinner');

                criteriaSpinner.removeClass('d-none');
                container.html('');

                if(diagnos_id != '') {
                    $.post('/backoffice/getCriteria', {
                        '_token': '{{ csrf_token() }}',
                        diagnos_id: diagnos_id
                    }, function (res) {
                        criteriaSpinner.addClass('d-none');
                        container.html(res);
                    })
                }
            }).change();
            
           // $('#diagnos').change();
        </script>
@endsection