@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Заявка</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Заявка</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <input type="hidden" id="application_id" value="{{ $application->id }}">
                        <select class="form-control custom-select status_app" id="status_app" name="status_app">
                            <option value="1" {{ $application->status == 1 ? 'selected' : ''}}>На рассмотрении</option>
                            <option value="2" {{ $application->status == 2 ? 'selected' : ''}}>Принята</option>
                            <option value="3" {{ $application->status == 3 ? 'selected' : ''}}>На доработке</option>
                            <option value="4" {{ $application->status == 4 ? 'selected' : ''}}>Отклонена</option>
                        </select>
                        <span></span>
                    </div>
                </div>
                <form class="form-horizontal" method="POST" action="{{ route('backoffice.application.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Конкурс</label>
                                <div class="col-lg-10">
                                    <p>{{ $application->indastry->name }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Система налогооблажения</label>
                                <div class="col-lg-10">
                                    <p>{{ $application->taxation_system->name }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="number_employees">Количество сотрудников</label>
                                <div class="col-lg-10">
                                    <p>{{ $application->number_employees }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="number_branches">Количество точек общественного питания</label>
                                <div class="col-lg-10">
                                    <p>{{ $application->number_branches }}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Сколько работаете в сфере общественного питания</label>
                                <div class="col-lg-10">
                                    <p>{{ $application->experience }}</p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label">Ваши клиенты</label>
                                <div class="col-lg-10">
                                    {{ $application->client->name }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-4 col-form-label" for="companies">ИНН компаний с которыми работаете более 3 мес.</label>
                                <div class="col-lg-10">
                                    @foreach($application->partners as $partner)
                                    <hr>
                                    <p>Наименование: {{ $partner->name }}</p>
                                    <p>ИНН: {{ $partner->inn }}</p>
                                    <p>Адрес: {{ $partner->address }}</p>
                                    <hr>
                                    @endforeach
                                </div>
                            </div>

                            <div class="mt-5">
                                <h4 class="header-title">Прикрепленные файлы</h4>
                            </div>
                            @if($application->commercial_proposal_file)
                            <p><a href="{{ route('backoffice.download', ['file' => $application->commercial_proposal_file]) }}">Коммерческое предложение</a></p>
                            @endif
                            @if($application->menu_file)
                            <p><a href="{{ route('backoffice.download', ['file' => $application->menu_file]) }}">Меню с выходом блюд</a></p>
                            @endif
                            @if($application->assortiment_menu_file)
                            <p><a href="{{ route('backoffice.download', ['file' => $application->assortiment_menu_file]) }}">Ассортимент меню</a></p>
                            @endif
                            @if($application->photo_set_lunch)
                            <p><a href="{{ route('backoffice.download', ['file' => $application->photo_set_lunch]) }}">Фото комплексного обеда (внешний вид, состав, упаковка)</a></p>
                            @endif
                        </div>
                    </div>
                </form>

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('.status_app').on('change', function() {
            var application_id = $("#application_id").val();
            var status_app = $(this).val();
            $.post('/backoffice/application/status', {
                application_id: application_id,
                status_app: status_app
            }, function() {
                $(this).siblings('span').html('ok');
            })
        });
    })
</script>
@endsection

@section('script-bottom')
@endsection