@extends('backoffice.layouts.vertical')


@section('css')
<!-- plugins -->
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

<!-- App css -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Аналитика</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Аналитика</h4>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <!-- stats + charts -->
                <div class="row">
                    <div class="col-xl-3">
                        <div class="card">
                            <div class="card-body p-0">
                                <h5 class="card-title header-title border-bottom p-3 mb-0">Стасистика</h5>
                                <!-- stat 1 -->
                                <div class="media px-3 py-4 border-bottom">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['all'] }}</h4>
                                        <span class="text-muted">Всего проверено</span>
                                    </div>
                                    <i data-feather="users" class="align-self-center icon-dual icon-lg"></i>
                                </div>

                                <!-- stat 2 -->
                                <div class="media px-3 py-4 border-bottom">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['criteria'] }}</h4>
                                        <span class="text-muted">Выявлено ошибок по критериям качества</span>
                                    </div>
                                    <i data-feather="image" class="align-self-center icon-dual icon-lg"></i>
                                </div>

                                <!-- stat 3 -->
                                <div class="media px-3 py-4">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['examinations'] }}</h4>
                                        <span class="text-muted">Выявлено ошибок по обследованиям</span>
                                    </div>
                                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                                </div>
                                <!-- stat 3 -->
                                <div class="media px-3 py-4">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['treatment'] }}</h4>
                                        <span class="text-muted">Выявлено ошибок по лечению</span>
                                    </div>
                                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-body pb-0">
                                <ul class="nav card-nav float-right">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#">Сегодня</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-muted" href="#">7д</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-muted" href="#">15д</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-muted" href="#">1м</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link text-muted" href="#">1г</a>
                                    </li>
                                </ul>
                                <h5 class="card-title mb-0 header-title">Динамика ошибок</h5>

                                <div id="revenue-chart" class="apex-charts mt-3" dir="ltr"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3">
                        <div class="card">
                            <div class="card-body pb-0">
                                <h5 class="card-title header-title">По месяцам</h5>
                                <div id="targets-chart" class="apex-charts mt-3" dir="ltr"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title mt-0 mb-0 header-title">Количество ошибок в разрезе ЛПУ</h5>
                                <div id="sales-by-category-chart" class="apex-charts mb-0 mt-4" dir="ltr"></div>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- optional plugins -->
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>


<!-- page js -->
<script src="{{ asset('assets/js/pages/dashboard.init.js') }}"></script>
<script>
    function getDaysInMonth(month, year) {
      var date = new Date(year, month, 1);
      var days = [];
      var idx = 0;

      while (date.getMonth() === month && idx < 15) {
        var d = new Date(date);
        days.push(d.getDate() + " " + d.toLocaleString('en-us', {
          month: 'short'
        }));
        date.setDate(date.getDate() + 1);
        idx += 1;
      }

      return days;
    }

    var now = new Date();
    var labels = getDaysInMonth(now.getMonth(), now.getFullYear());
    var options = {
        chart: {
            height: 296,
            type: 'area'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth',
            width: 4
        },
        series: [{
            name: 'Ошибок',
            data: '{{ implode("," , $count["dynamic"]) }}'.split(',')
        }],
        zoom: {
            enabled: false
        },
        legend: {
            show: false
        },
        colors: ['#43d39e'],
        xaxis: {
            type: 'string',
            categories: labels,
            tooltip: {
                enabled: false
            },
            axisBorder: {
                show: false
            },
            labels: {}
        },
        yaxis: {
            labels: {
                formatter: function formatter(val) {
                    return val;
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                type: "vertical",
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [45, 100]
            }
        }
    };
    var chart = new ApexCharts(document.querySelector("#revenue-chart"), options);
    chart.render();

    var options = {
      chart: {
        height: 296,
        type: 'bar',
        stacked: true,
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '45%'
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [
      //   {
      //   name: 'Net Profit',
      //   data: [35, 44, 55, 57, 56, 61]
      // }, 
      {
        name: 'Revenue',
        data: [0, parseInt('{{ $count["allError"] }}'), 0]
      }],
      xaxis: {
        categories: ['Май', 'Июнь', 'Июль'],
        axisBorder: {
          show: false
        }
      },
      legend: {
        show: false
      },
      grid: {
        row: {
          colors: ['transparent', 'transparent'],
          // takes an array which will be repeated on columns
          opacity: 0.2
        },
        borderColor: '#f3f4f7'
      },
      tooltip: {
        y: {
          formatter: function formatter(val) {
            return val + 'ошибок';
          }
        }
      }
    };
    var chart = new ApexCharts(document.querySelector("#targets-chart"), options);
    chart.render(); // sales by category --------------------------------------------------


    var options = {
      plotOptions: {
        pie: {
          donut: {
            size: '70%'
          },
          expandOnClick: false
        }
      },
      chart: {
        height: 298,
        type: 'donut'
      },
      legend: {
        show: true,
        position: 'right',
        horizontalAlign: 'left',
        itemMargin: {
          horizontal: 6,
          vertical: 3
        }
      },
      series: [parseInt('{{ $count["noyabr"] }}'), parseInt('{{ $count["urengoy"] }}')],
      labels: ['Ноябрьская ЦГБ', 'Новоуренгойская ЦГБ'],
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            position: 'bottom'
          }
        }
      }],
      tooltip: {
        y: {
          formatter: function formatter(value) {
            return value;
          }
        }
      }
    };
    var chart = new ApexCharts(document.querySelector("#sales-by-category-chart"), options);
    chart.render();
</script>
@endsection