@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<!-- <div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Доступ запрещен</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Пользователи</h4>
    </div>
</div> -->
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h1>Доступ запрещен</h1>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- Datatables init -->
@endsection