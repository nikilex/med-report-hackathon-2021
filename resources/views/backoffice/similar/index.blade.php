@extends('backoffice.layouts.vertical')


@section('css')
  <!-- Plugins css -->
  <link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
        <link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Критерии</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Критерии</h4>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h2>Прикрепление схожих критериев</h2>

                        <div class="spinner-border text-primary m-2 d-none" id="spinner" role="status">
                                    <span class="sr-only">Loading...</span>
                                </div>
                        <div class="row mt-3">
                            <div class="col">
                                <select data-plugin="customselect" class="form-control" id="diagnos" name="diagnos">
                                    <option value='' selected>Выберите диагноз</option>
                                    @foreach($diagnosis as $diagnos)
                                    <option value="{{ $diagnos->id }}">{{ $diagnos->code }} {{ $diagnos->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col mt-3">
                                <select data-plugin="customselect" class="form-control" id="diagnos_exam" name="diagnos">
                                    <option value='' selected>Выберите критерий обследования</option>
                                </select>
                                <div class="mt-3">
                                    <label for="exampleFormControlInput1" class="form-label">Напишите критерий</label>
                                    <input type="text" class="form-control" id="new_exam" placeholder="">
                                </div>
                                <button class="btn btn-primary mt-3" id="saveExam">Сохранить</button>
                            </div>
                            <div class="col mt-3">
                                <select data-plugin="customselect" class="form-control" id="diagnos_treat" name="diagnos">
                                    <option value='' selected>Выберите критерий лечения</option>
                                </select>
                                <div class="mt-3">
                                    <label for="exampleFormControlInput1" class="form-label">Напишите критерий</label>
                                    <input type="text" class="form-control" id="new_treat" placeholder="name@.com">
                                </div>
                                <button class="btn btn-primary mt-3" id="saveTreat">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('script')
 <!-- Plugins Js -->
 <script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
        <script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
        <script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
        <script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
        
        <!-- Init js-->
        <script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection

@section('script-bottom')
<script>
    $('#saveExam').on('click', function () {
        console.log('here');
        let diagnos_id = $('#diagnos option:selected');
        let diagnos_exam_id = $('#diagnos_exam option:selected');
        let new_exam = $('#new_exam');

        $.post('/backoffice/similar/saveExam', {
                '_token': '{{ csrf_token() }}',
                diagnos_id: diagnos_id.val(),
                diagnos_exam_id: diagnos_exam_id.val(),
                new_exam: new_exam.val(),
                type: 'examination'
            }, function(res) {
               
                console.log(res);
                //container.html(res);
            })
    })

    $('#saveTreat').on('click', function () {
        console.log('here');
        let diagnos_id = $('#diagnos option:selected');
        let diagnos_exam_id = $('#diagnos_treat option:selected');
        let new_exam = $('#new_treat');

        $.post('/backoffice/similar/saveExam', {
                '_token': '{{ csrf_token() }}',
                diagnos_id: diagnos_id.val(),
                diagnos_exam_id: diagnos_exam_id.val(),
                new_exam: new_exam.val(),
                type: 'traitment'
            }, function(res) {
               
                console.log(res);
                //container.html(res);
            })
    })

    $('#diagnos').on('change', function() {
        let diagnos_id = $('#diagnos option:selected').val();
       // let container = $('#criteria');
        let spinner = $('#spinner');
        let diagnos_exam = $('#diagnos_exam');
        let diagnos_treat = $('#diagnos_treat');

        spinner.removeClass('d-none');
       // container.html('');

        if (diagnos_id != '') {
            $.post('/backoffice/similar/getExaminations', {
                '_token': '{{ csrf_token() }}',
                diagnos_id: diagnos_id
            }, function(res) {
                spinner.addClass('d-none');
                diagnos_exam.html("<option value='' selected>Выберите диагноз</option>'");
                diagnos_treat.html("<option value='' selected>Выберите диагноз</option>'");
                for (examination of res.examinations) {
                        console.log(examination);
                        if (examination.type == 'examination') {
                            diagnos_exam.append('<option value="'+ examination.id +'">'+ examination.name +'</option>')
                        }
                        if (examination.type == 'treatment') {
                            diagnos_treat.append('<option value="'+ examination.id +'">'+ examination.name +'</option>')
                        }
                }
                console.log(res);
                //container.html(res);
            })
        }
    })

    // $('#diagnos').change();
</script>
@endsection