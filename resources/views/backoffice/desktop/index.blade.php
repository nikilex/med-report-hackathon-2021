@extends('backoffice.layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />

<!-- plugins -->
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

<!-- App css -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Рабочий стол</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Рабочий стол</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">Сегодня проверено</span>
                        <h2 class="mb-0">2189</h2>
                    </div>
                    <div class="align-self-center">
                        <div id="today-revenue-chart" class="apex-charts"></div>
                        <!-- <span class="text-success font-weight-bold font-size-13"><i class='uil uil-arrow-up'></i>
                            10.21%</span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">Всего проверено</span>
                        <h2 class="mb-0">1065</h2>
                    </div>
                    <div class="align-self-center">
                        <div id="today-product-sold-chart" class="apex-charts"></div>
                        <!-- <span class="text-danger font-weight-bold font-size-13"><i class='uil uil-arrow-down'></i>
                            5.05%</span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">Сегодня ошибок </span>
                        <h2 class="mb-0">11</h2>
                    </div>
                    <div class="align-self-center">
                        <div id="today-new-customer-chart" class="apex-charts"></div>
                        <!-- <span class="text-success font-weight-bold font-size-13"><i class='uil uil-arrow-up'></i>
                            25.16%</span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">Всего ошибок</span>
                        <h2 class="mb-0">750</h2>
                    </div>
                    <div class="align-self-center">
                        <div id="today-new-visitors-chart" class="apex-charts"></div>
                        <!-- <span class="text-danger font-weight-bold font-size-13"><i class='uil uil-arrow-down'></i>
                            5.05%</span> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- stats + charts -->
<div class="row">
    <div class="col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <h5 class="card-title header-title border-bottom p-3 mb-0">Стасистика</h5>
                <!-- stat 1 -->
                <div class="media px-3 py-4 border-bottom">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['all'] }}</h4>
                        <span class="text-muted">Всего проверено</span>
                    </div>
                    <i data-feather="users" class="align-self-center icon-dual icon-lg"></i>
                </div>

                <!-- stat 2 -->
                <div class="media px-3 py-4 border-bottom">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['criteria'] }}</h4>
                        <span class="text-muted">Выявлено ошибок по критериям качества</span>
                    </div>
                    <i data-feather="image" class="align-self-center icon-dual icon-lg"></i>
                </div>

                <!-- stat 3 -->
                <div class="media px-3 py-4">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['examinations'] }}</h4>
                        <span class="text-muted">Выявлено ошибок по обследованиям</span>
                    </div>
                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                </div>
                <!-- stat 3 -->
                <div class="media px-3 py-4">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $count['treatment'] }}</h4>
                        <span class="text-muted">Выявлено ошибок по лечению</span>
                    </div>
                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-6">
        <div class="card">
            <div class="card-body pb-0">
                <ul class="nav card-nav float-right">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Сегодня</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="#">7д</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="#">15д</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="#">1м</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-muted" href="#">1г</a>
                    </li>
                </ul>
                <h5 class="card-title mb-0 header-title">Динамика ошибок</h5>

                <div id="revenue-chart" class="apex-charts mt-3" dir="ltr"></div>
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <div class="card">
            <div class="card-body pb-0">
                <h5 class="card-title header-title">По месяцам</h5>
                <div id="targets-chart" class="apex-charts mt-3" dir="ltr"></div>
            </div>
        </div>
    </div>
</div>
<!-- row -->

<!-- products -->
<div class="row">
    <div class="col-xl-5">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mt-0 mb-0 header-title">Количество ошибок в разрезе ЛПУ</h5>
                <div id="sales-by-category-chart" class="apex-charts mb-0 mt-4" dir="ltr"></div>
            </div> 
        </div> 
    </div> 
    <!-- <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
                <a href="" class="btn btn-primary btn-sm float-right">
                    <i class='uil uil-export ml-1'></i> Export
                </a>
                <h5 class="card-title mt-0 mb-0 header-title">Recent Orders</h5>

                <div class="table-responsive mt-4">
                    <table class="table table-hover table-nowrap mb-0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Customer</th>
                                <th scope="col">Price</th>
                                <th scope="col">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>#98754</td>
                                <td>ASOS Ridley High</td>
                                <td>Otto B</td>
                                <td>$79.49</td>
                                <td><span class="badge badge-soft-warning py-1">Pending</span></td>
                            </tr>
                            <tr>
                                <td>#98753</td>
                                <td>Marco Lightweight Shirt</td>
                                <td>Mark P</td>
                                <td>$125.49</td>
                                <td><span class="badge badge-soft-success py-1">Delivered</span>
                                </td>
                            </tr>
                            <tr>
                                <td>#98752</td>
                                <td>Half Sleeve Shirt</td>
                                <td>Dave B</td>
                                <td>$35.49</td>
                                <td><span class="badge badge-soft-danger py-1">Declined</span>
                                </td>
                            </tr>
                            <tr>
                                <td>#98751</td>
                                <td>Lightweight Jacket</td>
                                <td>Shreyu N</td>
                                <td>$49.49</td>
                                <td><span class="badge badge-soft-success py-1">Delivered</span>
                                </td>
                            </tr>
                            <tr>
                                <td>#98750</td>
                                <td>Marco Shoes</td>
                                <td>Rik N</td>
                                <td>$69.49</td>
                                <td><span class="badge badge-soft-danger py-1">Declined</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            </div> 
        </div> 
    </div>  -->
</div>


<!-- widgets -->
<!-- <div class="row">
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body pt-2">
                <h5 class="mb-4 header-title">Top Performers</h5>
                <div class="media border-top pt-3">
                    <img src="assets/images/users/avatar-7.jpg" class="avatar rounded mr-3" alt="shreyu">
                    <div class="media-body">
                        <h6 class="mt-1 mb-0 font-size-15">Shreyu N</h6>
                        <h6 class="text-muted font-weight-normal mt-1 mb-3">Senior Sales Guy</h6>
                    </div>
                    <div class="dropdown align-self-center float-right">
                        <a href="#" class="dropdown-toggle arrow-none text-muted" data-toggle="dropdown" aria-expanded="false">
                            <i class="uil uil-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-edit-alt mr-2"></i>Edit</a>
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-exit mr-2"></i>Remove
                                from Team</a>
                            <div class="dropdown-divider"></div>
                            
                            <a href="javascript:void(0);" class="dropdown-item text-danger"><i class="uil uil-trash mr-2"></i>Delete</a>
                        </div>
                    </div>
                </div>
                <div class="media mt-1 border-top pt-3">
                    <img src="assets/images/users/avatar-9.jpg" class="avatar rounded mr-3" alt="shreyu">
                    <div class="media-body">
                        <h6 class="mt-1 mb-0 font-size-15">Greeva Y</h6>
                        <h6 class="text-muted font-weight-normal mt-1 mb-3">Social Media Campaign</h6>
                    </div>
                    <div class="dropdown align-self-center float-right">
                        <a href="#" class="dropdown-toggle arrow-none text-muted" data-toggle="dropdown" aria-expanded="false">
                            <i class="uil uil-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-edit-alt mr-2"></i>Edit</a>
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-exit mr-2"></i>Remove
                                from Team</a>
                            <div class="dropdown-divider"></div>
                            
                            <a href="javascript:void(0);" class="dropdown-item text-danger"><i class="uil uil-trash mr-2"></i>Delete</a>
                        </div>
                    </div>
                </div>
                <div class="media mt-1 border-top pt-3">
                    <img src="assets/images/users/avatar-4.jpg" class="avatar rounded mr-3" alt="shreyu">
                    <div class="media-body">
                        <h6 class="mt-1 mb-0 font-size-15">Nik G</h6>
                        <h6 class="text-muted font-weight-normal mt-1 mb-3">Inventory Manager</h6>
                    </div>
                    <div class="dropdown align-self-center float-right">
                        <a href="#" class="dropdown-toggle arrow-none text-muted" data-toggle="dropdown" aria-expanded="false">
                            <i class="uil uil-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-edit-alt mr-2"></i>Edit</a>
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-exit mr-2"></i>Remove
                                from Team</a>
                            <div class="dropdown-divider"></div>
                            
                            <a href="javascript:void(0);" class="dropdown-item text-danger"><i class="uil uil-trash mr-2"></i>Delete</a>
                        </div>
                    </div>
                </div>
                <div class="media mt-1 border-top pt-3">
                    <img src="assets/images/users/avatar-1.jpg" class="avatar rounded mr-3" alt="shreyu">
                    <div class="media-body">
                        <h6 class="mt-1 mb-0 font-size-15">Hardik G</h6>
                        <h6 class="text-muted font-weight-normal mt-1 mb-3">Sales Person</h6>
                    </div>
                    <div class="dropdown align-self-center float-right">
                        <a href="#" class="dropdown-toggle arrow-none text-muted" data-toggle="dropdown" aria-expanded="false">
                            <i class="uil uil-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-edit-alt mr-2"></i>Edit</a>
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-exit mr-2"></i>Remove
                                from Team</a>
                            <div class="dropdown-divider"></div>
                            
                            <a href="javascript:void(0);" class="dropdown-item text-danger"><i class="uil uil-trash mr-2"></i>Delete</a>
                        </div>
                    </div>
                </div>

                <div class="media mt-1 border-top pt-3">
                    <img src="assets/images/users/avatar-5.jpg" class="avatar rounded mr-3" alt="shreyu">
                    <div class="media-body">
                        <h6 class="mt-1 mb-0 font-size-15">Stive K</h6>
                        <h6 class="text-muted font-weight-normal mt-1 mb-1">Sales Person</h6>
                    </div>
                    <div class="dropdown align-self-center float-right">
                        <a href="#" class="dropdown-toggle arrow-none text-muted" data-toggle="dropdown" aria-expanded="false">
                            <i class="uil uil-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-edit-alt mr-2"></i>Edit</a>
                            
                            <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-exit mr-2"></i>Remove
                                from Team</a>
                            <div class="dropdown-divider"></div>
                            
                            <a href="javascript:void(0);" class="dropdown-item text-danger"><i class="uil uil-trash mr-2"></i>Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body pt-2 pb-3">
                <a href="task-list.html" class="btn btn-primary btn-sm mt-2 float-right">
                    View All
                </a>
                <h5 class="mb-4 header-title">Tasks</h5>
                <div class="slimscroll" style="max-height: 390px;">
                    <div class="row">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task1">
                                <label class="custom-control-label" for="task1">
                                    Draft the new contract document for
                                    sales team
                                </label>
                                <p class="font-size-13 text-muted">Due on 24 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task2">
                                <label class="custom-control-label" for="task2">
                                    iOS App home page
                                </label>
                                <p class="font-size-13 text-muted">Due on 23 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task3">
                                <label class="custom-control-label" for="task3">
                                    Write a release note for Shreyu
                                </label>
                                <p class="font-size-13 text-muted">Due on 22 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task4">
                                <label class="custom-control-label" for="task4">
                                    Invite Greeva to a project shreyu admin
                                </label>
                                <p class="font-size-13 text-muted">Due on 21 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task5">
                                <label class="custom-control-label" for="task5">
                                    Enable analytics tracking for main website
                                </label>
                                <p class="font-size-13 text-muted">Due on 20 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task6">
                                <label class="custom-control-label" for="task6">
                                    Invite user to a project
                                </label>
                                <p class="font-size-13 text-muted">Due on 18 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="task7">
                                <label class="custom-control-label" for="task7">
                                    Write a release note
                                </label>
                                <p class="font-size-13 text-muted">Due on 14 Aug, 2019</p>
                            </div> 
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body pt-2">
                <div class="dropdown mt-2 float-right">
                    <a href="#" class="dropdown-toggle arrow-none text-muted" data-toggle="dropdown" aria-expanded="false">
                        <i class="uil uil-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        
                        <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-refresh mr-2"></i>Refresh</a>
                        
                        <a href="javascript:void(0);" class="dropdown-item"><i class="uil uil-user-plus mr-2"></i>Add
                            Member</a>
                        <div class="dropdown-divider"></div>
                        
                        <a href="javascript:void(0);" class="dropdown-item text-danger"><i class="uil uil-exit mr-2"></i>Exit</a>
                    </div>
                </div>
                <h5 class="mb-4 header-title">Recent Conversation</h5>
                <div class="chat-conversation">
                    <ul class="conversation-list slimscroll" style="max-height: 328px;">
                        <li class="clearfix">
                            <div class="chat-avatar">
                                <img src="assets/images/users/avatar-9.jpg" alt="Female">
                                <i>10:00</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>Greeva</i>
                                    <p>
                                        Hello!
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix odd">
                            <div class="chat-avatar">
                                <img src="assets/images/users/avatar-7.jpg" alt="Male">
                                <i>10:01</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>Shreyu</i>
                                    <p>
                                        Hi, How are you? What about our next meeting?
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix">
                            <div class="chat-avatar">
                                <img src="assets/images/users/avatar-9.jpg" alt="female">
                                <i>10:01</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>Greeva</i>
                                    <p>
                                        Yeah everything is fine
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="clearfix odd">
                            <div class="chat-avatar">
                                <img src="assets/images/users/avatar-7.jpg" alt="male">
                                <i>10:02</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>Shreyu</i>
                                    <p>
                                        Awesome! let me know if we can talk in 20 min
                                    </p>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <form class="needs-validation" novalidate name="chat-form" id="chat-form">
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control chat-input" placeholder="Enter your text" required>
                                <div class="invalid-feedback">
                                    Please enter your messsage
                                </div>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-danger chat-send btn-block waves-effect waves-light">Send</button>
                            </div>
                        </div>
                    </form>

                </div> 
            </div>
        </div>
    </div>
</div> -->

@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- Plugins Js -->
<script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

<!-- optional plugins -->
<script src="{{ asset('assets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/libs/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>

<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

<script>
    $('#diagnos').on('change', function() {
        let diagnos_id = $('#diagnos option:selected').val();
        let container = $('#criteria');
        let criteriaSpinner = $('#criteriaSpinner');

        criteriaSpinner.removeClass('d-none');
        container.html('');

        if (diagnos_id != '') {
            $.post('/backoffice/getCriteria', {
                '_token': '{{ csrf_token() }}',
                diagnos_id: diagnos_id
            }, function(res) {
                criteriaSpinner.addClass('d-none');
                container.html(res);
            })
        }
    })
</script>
<script>
    function getDaysInMonth(month, year) {
      var date = new Date(year, month, 1);
      var days = [];
      var idx = 0;

      while (date.getMonth() === month && idx < 15) {
        var d = new Date(date);
        days.push(d.getDate() + " " + d.toLocaleString('en-us', {
          month: 'short'
        }));
        date.setDate(date.getDate() + 1);
        idx += 1;
      }

      return days;
    }

    var now = new Date();
    var labels = getDaysInMonth(now.getMonth(), now.getFullYear());
    var options = {
        chart: {
            height: 296,
            type: 'area'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth',
            width: 4
        },
        series: [{
            name: 'Ошибок',
            data: '{{ implode("," , $count["dynamic"]) }}'.split(',')
        }],
        zoom: {
            enabled: false
        },
        legend: {
            show: false
        },
        colors: ['#43d39e'],
        xaxis: {
            type: 'string',
            categories: labels,
            tooltip: {
                enabled: false
            },
            axisBorder: {
                show: false
            },
            labels: {}
        },
        yaxis: {
            labels: {
                formatter: function formatter(val) {
                    return val;
                }
            }
        },
        fill: {
            type: 'gradient',
            gradient: {
                type: "vertical",
                shadeIntensity: 1,
                inverseColors: false,
                opacityFrom: 0.45,
                opacityTo: 0.05,
                stops: [45, 100]
            }
        }
    };
    var chart = new ApexCharts(document.querySelector("#revenue-chart"), options);
    chart.render();

    var options = {
      chart: {
        height: 296,
        type: 'bar',
        stacked: true,
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '45%'
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [
      //   {
      //   name: 'Net Profit',
      //   data: [35, 44, 55, 57, 56, 61]
      // }, 
      {
        name: 'Revenue',
        data: [0, parseInt('{{ $count["allError"] }}'), 0]
      }],
      xaxis: {
        categories: ['Май', 'Июнь', 'Июль'],
        axisBorder: {
          show: false
        }
      },
      legend: {
        show: false
      },
      grid: {
        row: {
          colors: ['transparent', 'transparent'],
          // takes an array which will be repeated on columns
          opacity: 0.2
        },
        borderColor: '#f3f4f7'
      },
      tooltip: {
        y: {
          formatter: function formatter(val) {
            return val + 'ошибок';
          }
        }
      }
    };
    var chart = new ApexCharts(document.querySelector("#targets-chart"), options);
    chart.render(); // sales by category --------------------------------------------------


    var options = {
      plotOptions: {
        pie: {
          donut: {
            size: '70%'
          },
          expandOnClick: false
        }
      },
      chart: {
        height: 298,
        type: 'donut'
      },
      legend: {
        show: true,
        position: 'right',
        horizontalAlign: 'left',
        itemMargin: {
          horizontal: 6,
          vertical: 3
        }
      },
      series: [parseInt('{{ $count["noyabr"] }}'), parseInt('{{ $count["urengoy"] }}')],
      labels: ['Ноябрьская ЦГБ', 'Новоуренгойская ЦГБ'],
      responsive: [{
        breakpoint: 480,
        options: {
          legend: {
            position: 'bottom'
          }
        }
      }],
      tooltip: {
        y: {
          formatter: function formatter(value) {
            return value;
          }
        }
      }
    };
    var chart = new ApexCharts(document.querySelector("#sales-by-category-chart"), options);
    chart.render();
</script>
@endsection