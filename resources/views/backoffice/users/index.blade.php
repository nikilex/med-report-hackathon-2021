@extends('backoffice.layouts.vertical')


@section('css')
<!-- plugin css -->
<link href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Plugins css -->
<link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
    type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Пользователи</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Пользователи</h4>
    </div>
</div>
@endsection

@section('content')
@if (session('success'))
<div class="row">
    <div class="col">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row justify-content-between">
                    <div class="col">
                        <a href="{{ route('backoffice.users.add') }}" class="btn btn-outline-primary mb-3">Добавить пользователя</a>
                    </div>
                    <div class="col-4">
                        <div class="form-group mt-3 mt-sm-0">
                            <select data-plugin="customselect" class="form-control cityFilter" data-placeholder="Фильтр по городам">
                                <option></option>
                                <option value="all">Все города</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id}}" {{ $city->id == $request->city ? 'selected' : ''}}>{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>


                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Эл. адрес</th>
                            <th>Город</th>
                            <th>Роль</th>
                            <th>Номер завки</th>
                            <th>Статус</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td> <a href="{{ route('backoffice.users.edit', ['id' => $user->id]) }}">{{ $user->name }}</a> </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->city->name }}</td>
                            <td>{{ $user->role->name }}</td>
                            <td class="application">
                                <select class="form-control custom-select application_id" name="application_id">
                                    @foreach($user->applications()->get() as $application)
                                    <option value="{{ $application->id }}">{{ $application->id }}</option>
                                    @endforeach
                                </select>
                            </td>

                            <td>
                                <select class="form-control custom-select status_app" name="status_app">
                                    <option value="1">На рассмотрении</option>
                                    <option value="2">Принята</option>
                                    <option value="3">На доработке</option>
                                    <option value="4">Отклонена</option>
                                </select>
                                <span class="result d-none"></span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->
@endsection

@section('script')
<!-- datatable js -->
<script src="{{ URL::asset('assets/libs/datatables/datatables.min.js') }}"></script>
<!-- Plugins Js -->
<script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.status_app').on('change', function() {
            var application_id = $(this).parent().siblings('.application').children('.application_id').val();
            var status_app = $(this).val();
            $.post('/backoffice/application/status', {
                application_id: application_id,
                status_app: status_app
            }, function() {
                $(this).siblings('span').html('ok');
            })
        });

        $('.cityFilter').on('change', function(){
            console.log(+$(this).val());
            if($(this).val() != 'all')
            {
                window.location = '/backoffice/users?city='+$(this).val();
            } else {
                window.location = '/backoffice/users';
            }
        });
    })
</script>
@endsection

@section('script-bottom')
<!-- Datatables init -->
<script src="{{ URL::asset('assets/js/pages/datatables.init.js') }}"></script>
<script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection