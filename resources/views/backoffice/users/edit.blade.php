@extends('backoffice.layouts.vertical')


@section('css')
<style>
    .eye,
    .eye-off {
        cursor: pointer;
    }
</style>
<!-- Plugins css -->
<link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="/">Пользователи</a></li>
                <li class="breadcrumb-item active" aria-current="page">Добавление пользователя</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Добавление пользователя</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Данные пользователя</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Заявки</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <form class="form-horizontal" method="POST" action="{{ route('backoffice.users.update') }}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="name">Имя</label>
                                        <div class="col-lg-10">
                                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $user->name }}">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="email">Эл. почта</label>
                                        <div class="col-lg-10">
                                            <input type="text" id="email" name="email" class="form-control @error('name') is-invalid @enderror" value="{{ $user->email }}" required>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Город</label>
                                        <div class="col-lg-10">
                                            <select data-plugin="customselect" class="form-control cityFilter @error('city') is-invalid @enderror" name="city">
                                                <option>Выберите</option>
                                                @foreach($cities as $city)
                                                <option value="{{ $city->id}}" {{ $user->city()->first()->id == $city->id  ? 'selected' : ''}}>{{ $city->name }}</option>
                                                @endforeach
                                            </select>
                                            @error('city')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label">Роль</label>
                                        <div class="col-lg-10">
                                            <select class="form-control custom-select @error('role') is-invalid @enderror" name="role" required>
                                                @foreach($roles as $role)
                                                @if($role->id != 1)
                                                <option value="{{ $role->id }}" {{ $user->role()->first()->id == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @error('role')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2 col-form-label" for="password">Пароль</label>
                                        <div class="col-lg-10">
                                            <div class="row  align-items-center">
                                                <div class="col">
                                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ $user->open_password }}" required>
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div class="col-2 text-center">
                                                    <i class="eye" data-feather="eye"></i>
                                                </div>
                                                <div class="col-2 d-none text-center">
                                                    <i class="eye-off" data-feather="eye-off"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-primary mb-3">Сохранить</button>
                                        <button type="button" class="btn btn-outline-danger mb-3" data-toggle="modal" data-target="#deleteModal">Удалить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <table id="basic-datatable" class="table dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th>Номер заявки</th>
                                    <th>Описание события</th>
                                    <th>Статус события</th>
                                </tr>
                            </thead>


                            <tbody>
                                @foreach($applications as $application)
                                <tr>
                                    <td> <a href="{{ route('backoffice.applications.application', ['id' => $application->id]) }}">{{ $application->id }}</a> </td>
                                    <td>{{ $application->indastry()->first()->name }}</td>
                                    <td> <span style="color: #c1c11f">{{ $application->status == 1 ? 'На рассмотрении' : ''  }}</span>
                                        <span style="color: green">{{ $application->status == 2 ? 'Принята' : ''  }}</span>
                                        <span style="color: gray"> {{ $application->status == 3 ? 'На доработке' : ''  }}</span>
                                        <span style="color: red"> {{ $application->status == 4 ? 'Отклонена' : ''  }}</span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->

<!-- sample modal content -->
<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Удаление пользователя</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Вы действительно хотите удалить этого пользователя?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                <form action="{{ route('backoffice.users.delete') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <button type="submit" class="btn btn-outline-danger">Удалить</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('.eye').on('click', function() {
            $('#password').attr('type', 'text');
            $('.eye-off').parent().removeClass('d-none');
            $(this).parent().addClass('d-none');
        });
        $('.eye-off').on('click', function() {
            $('#password').attr('type', 'password');
            $('.eye').parent().removeClass('d-none');
            $(this).parent().addClass('d-none');
        });
    })
</script>
<!-- Plugins Js -->
<script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('script-bottom')
<script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection