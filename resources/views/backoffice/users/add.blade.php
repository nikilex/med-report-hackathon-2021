@extends('backoffice.layouts.vertical')


@section('css')
<style>
    .eye,
    .eye-off {
        cursor: pointer;
    }
</style>
<!-- Plugins css -->
<link href="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/multiselect/multiselect.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.css') }}" rel="stylesheet"
    type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="/">Пользователи</a></li>
                <li class="breadcrumb-item active" aria-current="page">Добавление пользователя</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Добавление пользователя</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{ route('backoffice.users.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="name">Имя</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="email">Эл. почта</label>
                                <div class="col-lg-10">
                                    <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Город</label>
                                <div class="col-lg-10">
                                    <select data-plugin="customselect" class="form-control cityFilter @error('city') is-invalid @enderror" name="city">
                                        <option>Выберите</option>
                                        @foreach($cities as $city)
                                        <option value="{{ $city->id}}" {{ $city->id == old('city') ? 'selected' : ''}}>{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Роль</label>
                                <div class="col-lg-10">
                                    <select class="form-control custom-select @error('role') is-invalid @enderror" name="role">
                                        @foreach($roles as $role)
                                        @if($role->id != 1)
                                        <option value="{{ $role->id }}" {{ $role->id == old('role') ? 'selected' : '' }}>{{ $role->name }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    @error('role')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="password">Пароль</label>
                                <div class="col-lg-10">
                                    <div class="row  align-items-center">
                                        <div class="col">
                                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-2 text-center">
                                            <i class="eye" data-feather="eye"></i>
                                        </div>
                                        <div class="col-2 d-none text-center">
                                            <i class="eye-off" data-feather="eye-off"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-primary mb-3">Добавить</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('.eye').on('click', function() {
            $('#password').attr('type', 'text');
            $('.eye-off').parent().removeClass('d-none');
            $(this).parent().addClass('d-none');
        });
        $('.eye-off').on('click', function() {
            $('#password').attr('type', 'password');
            $('.eye').parent().removeClass('d-none');
            $(this).parent().addClass('d-none');
        });
    })
</script>
<!-- Plugins Js -->
<script src="{{ URL::asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/multiselect/multiselect.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/libs/bootstrap-touchspin/bootstrap-touchspin.min.js') }}"></script>
@endsection

@section('script-bottom')
<script src="{{ URL::asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection