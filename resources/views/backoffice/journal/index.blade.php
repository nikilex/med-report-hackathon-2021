@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Журнал</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Журнал</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <h4 class="header-title mt-0 mb-2">Журнал проведённых экспертиз</h4>
                <table class="table table-striped  nowrap">
                    <thead>
                        <tr>
                            <th>№ обследования</th>
                            <th>Ошибок критериев</th>
                            <th>Ошибок обследования</th>
                            <th>Ошибок лечения</th>
                            <th>Всего ошибок</th>
                            <th>Результат</th>
                        </tr>
                    </thead>


                    <tbody>
                        @foreach($experts as $expert)
                            <tr>
                                <td>{{ $expert->id }}</td>
                                <td>{{ $expert->count_error_criteria }}</td>
                                <td>{{ $expert->count_error_examination }}</td>
                                <td>{{ $expert->count_error_treatment }}</td>
                                <td>{{ $expert->count_error_criteria + $expert->count_error_examination + $expert->count_error_treatment }}</td>
                                <th><a href="{{ route('backoffice.expertise.expert.view', ['id' => $expert->id]) }}">Посмотреть</a>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $experts->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<!-- end row-->


@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- datatable js -->
<script src="{{ asset('assets/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/libs/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/buttons.print.min.js') }}"></script>

<script src="{{ asset('assets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('assets/libs/datatables/dataTables.select.min.js') }}"></script>

<!-- Datatables init -->
<script src="{{ asset('assets/js/pages/datatables.init.js') }}"></script>
@endsection