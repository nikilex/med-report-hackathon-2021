@extends('backoffice.layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="{{ route('backoffice.diagnoses.index') }}">Диагнозы</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $examinations->first() ? ($examinations->first()->type == 'examination' ? 'Обследование' : 'Лечение') : ''}} </li>
            </ol>
        </nav>
        <div class="row p-0">
            <div class="col-auto">
                <a href="{{ route('backoffice.diagnoses.index') }}">
                    <i data-feather="arrow-left"></i>
                </a>
            </div>
            <div class="col-auto">
                <h4 class="mb-1 mt-0">{{ $examinations->first() ? ($examinations->first()->type == 'examination' ? 'Обследование' : 'Лечение') : ''}}</h4>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h4 class="header-title mt-0 mb-2">{{ $examinations->first() ? ($examinations->first()->type == 'examination' ? 'Обследования' : 'Лечение') : ''}} диагноза {{ $diagnos->name }}</h4>
                        <table class="table table-striped mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Наименование</th>
                                    <th scope="col">Опции</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($examinations)
                                @foreach($examinations as $key => $examination)
                                <tr>
                                    <th scope="row">{{ ($key+1) + ($examinations->currentPage() - 1)*$examinations->perPage() }}</th>
                                    <td>{{ $examination->name }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-soft-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                                        <polyline points="6 9 12 15 18 9"></polyline>
                                                    </svg></i>
                                            </button>
                                            <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -162px, 0px);">
                                                <a class="dropdown-item" href="{{ route('backoffice.examinations.edit', ['id' => $examination->id]) }}">Редактировать</a>
                                                <!-- <a class="dropdown-item" href="{{ route('backoffice.diagnoses.similar', ['id' => $examination->id]) }}">Синонимы</a> -->
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        <div class="mt-3">
                            {{ $examinations->links('vendor.pagination.bootstrap-4') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-2">Добавить {{ $examinations->first() ? ($examinations->first()->type == 'examination' ? 'обследование' : 'лечение') : ''}}</h4>
                <form action="{{ route('backoffice.examinations.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="examinationType" value="{{ $examinations->first() ? $examinations->first()->type : ''}} ">
                    <input type="hidden" name="diagnosId" value="{{ $diagnos->id }}">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Наименование</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="exam_name">
                        </div>
                    </div>
                    <h4 class="font-size-15 mt-3">Синонимы</h4>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Наименование</label>
                        <div id="similarBox" class="col-lg-10">
                            <input type="text" class="form-control" name="similar[]">
                        </div>
                        <div class="row col justify-content-end">
                            <button type="button" id="addSimilarBtn" class="btn btn-info btn-sm text-right">Добавить синоним</button>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить обследование</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- Plugins Js -->
<script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection

@section('script-bottom')
<script>
    $('#addSimilarBtn').on('click', function() {
        var similarBox = $('#similarBox');

        var similarInput = $('<input>', {
            type: 'text',
            name: 'similar[]',
            class: 'form-control my-2'
        })

        similarBox.append(similarInput);
    })
</script>
@endsection