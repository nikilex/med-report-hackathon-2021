@extends('backoffice.layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Диагнозы</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Диагнозы</h4>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-2">Все диагнозы</h4>
                <table class="table table-striped mb-0">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Код</th>
                            <th scope="col">Диагноз</th>
                            <th scope="col">Опции</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($diagnoses as $key => $diagnos)
                        <tr>
                            <th scope="row">{{ ($key+1) + ($diagnoses->currentPage() - 1)*$diagnoses->perPage() }}</th>
                            <td>{{ $diagnos->code }}</td>
                            <td>{{ $diagnos->name }}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-soft-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <i class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down">
                                                <polyline points="6 9 12 15 18 9"></polyline>
                                            </svg></i>
                                    </button>
                                    <div class="dropdown-menu" x-placement="top-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, -162px, 0px);">
                                        <a class="dropdown-item" href="{{ route('backoffice.diagnoses.edit', ['id' => $diagnos->id]) }}">Редактировать</a>
                                        <a class="dropdown-item" href="{{ route('backoffice.diagnoses.examinations', ['id' => $diagnos->id]) }}">Обследования</a>
                                        <a class="dropdown-item" href="{{ route('backoffice.diagnoses.treatments', ['id' => $diagnos->id]) }}">Лечение</a>
                                        <!-- <a class="dropdown-item" href="{{ route('backoffice.diagnoses.criterias', ['id' => $diagnos->id]) }}">Критерии</a> -->
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="mt-3">
                    {{ $diagnoses->links('vendor.pagination.bootstrap-4') }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-2">Добавить диагноз</h4>
                <form action="{{ route('backoffice.diagnoses.store') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Код диагноза</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="code" placeholder="Код диагноза">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Диагноз</label>
                        <div class="col-lg-10">
                            <input type="text" name="diagnos" class="form-control" placeholder="Диагноз">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- Plugins Js -->
<script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

<script>

</script>
@endsection