@extends('backoffice.layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="{{ route('backoffice.diagnoses.examinations', ['id' => $examination->diagnosis_id]) }}">{{ $examination->type == 'examination' ? 'Обследования' : 'Лечение'}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">Редактировать {{ $examination->type == 'examination' ? 'обследования' : 'Лечение'}} </li>
            </ol>
        </nav>
        <div class="row p-0">
            <div class="col-auto">
                <a href="{{ route('backoffice.diagnoses.examinations', ['id' => $examination->diagnosis_id]) }}">
                    <i data-feather="arrow-left"></i>
                </a>
            </div>
            <div class="col-auto">
                <h4 class="mb-1 mt-0">Редактировать {{ $examination->type == 'examination' ? 'обследования' : 'Лечение'}}</h4>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-2">Редактировать {{ $examination->type == 'examination' ? 'обследование' : 'лечение' }}</h4>
                <form action="{{ route('backoffice.examinations.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="examinationType" value="{{ $examination->type }} ">
                    <input type="hidden" name="diagnosId" value="{{ $examination->diagnosis_id }}">
                    <input type="hidden" name="examId" value="{{ $examination->id }}">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Наименование</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="exam_name" value="{{ $examination->name }}">
                        </div>
                    </div>
                    <h4 class="font-size-15 mt-3">Синонимы</h4>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Наименование</label>
                        <div id="similarBox" class="col-lg-10">
                            @foreach($similar as $value)
                            <input type="hidden" name="similarId[]" value="{{ $value->id }}">
                            <input type="text" class="form-control my-2" name="similar[]" value="{{ $value->name }}">
                            @endforeach
                        </div>
                        <div class="row col justify-content-end p-0">
                            <button type="button" id="addSimilarBtn" class="btn btn-info btn-sm text-right">Добавить синоним</button>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<!-- Plugins Js -->
<script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>
@endsection

@section('script-bottom')
<script>
    $('#addSimilarBtn').on('click', function() {
        var similarBox = $('#similarBox');

        var similarInput = $('<input>', {
            type: 'text',
            name: 'similar[]',
            class: 'form-control my-2'
        })

        similarBox.append(similarInput);
    })

    $('#saveTreat').on('click', function() {
        console.log('here');
        let diagnos_id = $('#diagnos option:selected');
        let diagnos_exam_id = $('#diagnos_treat option:selected');
        let new_exam = $('#new_treat');

        $.post('/backoffice/similar/saveExam', {
            '_token': '{{ csrf_token() }}',
            diagnos_id: diagnos_id.val(),
            diagnos_exam_id: diagnos_exam_id.val(),
            new_exam: new_exam.val(),
            type: 'traitment'
        }, function(res) {

            console.log(res);
            //container.html(res);
        })
    })

    // $('#diagnos').change();
</script>
@endsection