@extends('backoffice.layouts.vertical')


@section('css')
<!-- Plugins css -->
<link href="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="{{ route('backoffice.diagnoses.index') }}">Диагнозы</a></li>
                <li class="breadcrumb-item active" aria-current="page">Редактирование диагноза</li>
            </ol>
        </nav>
        <div class="row p-0">
            <div class="col-auto">
                <a href="{{ route('backoffice.diagnoses.index') }}">
                    <i data-feather="arrow-left"></i>
                </a>
            </div>
            <div class="col-auto">
                <h4 class="mb-1 mt-0">Редактирование диагноза</h4>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="row">
    <div class="col-12 col-md-6">
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-2">Редактирование диагноза</h4>
                <form action="{{ route('backoffice.diagnoses.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="diagnosId" value="{{ $diagnos->id }}">
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Код диагноза</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="code" value="{{ $diagnos->code }}" placeholder="Код диагноза">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-2 col-form-label" for="">Диагноз</label>
                        <div class="col-lg-10">
                            <input type="text" name="diagnos" class="form-control" value="{{ $diagnos->name }}" placeholder="Диагноз">
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <dov class="col-auto">
                            <button type="submit" class="btn btn-danger" id="deleteButton" data-toggle="modal" data-target="#deleteModal">Удалить</button>
                        </dov>
                        <dov class="col-auto">
                            <button type="submit" class="btn btn-primary">Обновить</button>
                        </dov>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Удаление записи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                Вы действительно хотите удалить эту запись?
            </div>
            <div class="modal-footer">
                <form action="{{ route('backoffice.diagnoses.delete') }}" method="post" id="deleteForm">
                    @csrf
                    <input type="hidden" name="diagnosId" value="{{ $diagnos->id }}">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-danger">Удалить</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


@endsection

@section('script')

@endsection

@section('script-bottom')
<!-- Plugins Js -->
<script src="{{ asset('assets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('assets/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/libs/multiselect/jquery.multi-select.js') }}"></script>
<script src="{{ asset('assets/libs/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>

<!-- Init js-->
<script src="{{ asset('assets/js/pages/form-advanced.init.js') }}"></script>

<script>
    $('deleteButton').on('click', function() {
        $()
    })
</script>
@endsection