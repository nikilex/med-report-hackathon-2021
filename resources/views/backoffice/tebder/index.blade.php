@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Конкурсы</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Конкурсы</h4>
    </div>
</div>
@endsection

@section('content')
@if (session('success'))
<div class="row">
    <div class="col">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>
                    Конкурс. Организация питания.
                </p>
                <p>Техническое задание:</p> 

                <p>Организация питания сотрудников склада.</p> 

                <p>Срок договора: 365 дней <br>
                Количество: 27 447 шт<br>
                Время доставки:<br>
                Будни: 11:30;20:30<br>
                Выходные: 11:30</p> 

                <p>Отсрочка платежа: 14 дней</p> 

                <p>Состав комплексного обеда:<br>
                Салат 100 гр.<br>
                Суп 250 гр<br>
                Мясное 100 гр<br>
                Гарнир 200 гр<br>
                Выпечка 100 гр</p> 

                <p>Всего участников (6)</p>
                <p>Допущены к тендеру (2)</p>

                <table id="basic-datatable" class="table dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th>Номер события</th>
                            <th>Описание события</th>
                            <th>Статус события</th>
                            <th>Номер предложения</th>
                            <th>Дата начала</th>
                            <th>Время начала</th>
                            <th>Дата окончания</th>
                            <th>Время окончания</th>
                        </tr>
                    </thead>


                    <tbody>

                        <tr>
                            <td>345345354</a> </td>
                            <td>Закупка 300 позиций продукции</td>
                            <td>Завершено</td>
                            <td>111341265</td>
                            <td>03.11.2020</td>
                            <td>15:00:00</td>
                            <td>05.12.2020</td>
                            <td>00:00:00</td>
                        </tr>

                    </tbody>
                </table>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')

@endsection

@section('script-bottom')
@endsection