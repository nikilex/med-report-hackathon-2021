@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Моя компания</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Моя компания</h4>
    </div>
</div>
@endsection

@section('content')
@if (session('success'))
<div class="row">
    <div class="col">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {!! session('success') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>
                    На этой странице можно посмотреть информацию о своей компании.
                </p>
                <a href="{{ route('backoffice.company.edit') }}" class="btn btn-outline-primary mb-3">Редактировать данные компании</a>
                <div class="row">
                    <div class="col-9">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="inn">Название</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="name" name="name" value="{{ $company->name }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="inn">ИНН</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="inn" name="inn" value="{{ $company->inn }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="address">Юридический адрес</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="address" name="address" value="{{ $company->address }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="public_name">Публичное название</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="public_name" name="public_name" value="{{ $company->public_name }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="actual_address">Фактический адресе</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="actual_address" name="actual_address" value="{{ $company->actual_address }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="responsible">Ответственный ФИО, должность</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="responsible" name="responsible" value="{{$company->responsible }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="phone">Телефон</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="phone" name="phone" value="{{ $company->phone }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="email">Эл. почта</label>
                            <div class="col-lg-10">
                                <input type="text" id="email" name="email" value="{{ $company->email }}" class="form-control" disabled>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-lg-2 col-form-label" for="industry">Отрасль</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="industry" name="industry" value="{{ $company->industry }}" disabled>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')

@endsection

@section('script-bottom')
@endsection