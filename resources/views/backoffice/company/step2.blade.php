@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Регистрация</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Регистрация шаг 2</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <p>
                    Для доступа к тендерной площадке необходимо заполнить данные Вашей компании.
                </p>
                <form class="form-horizontal" method="POST" action="{{ route('backoffice.company.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn">ИНН</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="number" class="form-control @error('inn') is-invalid @enderror" id="inn" name="inn" value="{{ old('inn') }}" data-toggle="dropdown">
                                    <div class="dropdown-menu dropdown-inn">
                                        <p class="decription ml-2">Выберите вариант или начните ввод</p>
                                        <div class="dadata">
                                            
                                        </div>
                                    </div>
                                    @error('inn')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <!-- <button type="button" class="btn btn-outline-info col-lg-3" id="sendInn">
                                    <div class="spinner-border text-dark d-none" style="width: 20px; height: 20px" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                    Заполнить по ИНН
                                </button> -->
                                <button type="button" class="btn btn-outline-danger col-lg-3 d-none" id="rewrite">Заполнить заново</button>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn">Название организации</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="address">Юридический адрес</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ old('address') }}">
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="public_name">Публичное название организации</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('public_name') is-invalid @enderror" id="public_name" name="public_name" value="{{ old('public_name') }}">
                                    @error('public_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="actual_address">Фактический адрес</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('actual_address') is-invalid @enderror" id="actual_address" name="actual_address" value="{{ old('actual_address') }}">
                                    @error('actual_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="responsible">Ответственный ФИО, должность</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('responsible') is-invalid @enderror" id="responsible" name="responsible" value="{{ old('responsible') }}">
                                    @error('responsible')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="phone">Телефон</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone') }}">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="email">Эл. почта</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" id="email" name="email" value="{{ old('email') }}" class="form-control form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="industry">Отрасль</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="text" class="form-control @error('industry') is-invalid @enderror" id="industry" name="industry" value="{{ old('industry') }}">
                                    @error('industry')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div> -->
                            <div class="mt-4">
                                <h4 class="header-title">Прикрепите файлы</h4>
                                <p class="sub-header">
                                    Все форматы изображений, текстовые word, pdf <br>
                                    Максимальный размер файла 10МБ
                                </p>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn_file">ИНН</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="file" class="form-control @error('inn_file') is-invalid @enderror" name="inn_file" id="inn_file">
                                    @error('inn_file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="ogrn_file">ОГРН (ОГРНИП)</label>
                                <div class="col-lg-10">
                                    <input autocomplete="off" type="file" class="form-control @error('ogrn_file') is-invalid @enderror" name="ogrn_file" id="ogrn_file">
                                    @error('ogrn_file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-primary mb-3">Завершить регистрацию</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        
        $('.dropdown-inn').on('click', '.dadata-item', function () {
            var inn = $(this).attr('data-inn');
            var address = $(this).attr('data-address');
            var name = $(this).attr('data-name');

            console.log(name);
            $('#inn').val(inn);
            $('#name').val(name);
            $('#address').val(address);
        })

        $('#inn').on('input', function() {
            var query = $(this);
            var dadataContainer = $('.dadata');
            $.post('/backoffice/company/searchDataByInn', {
                        query: query.val()
                    }, function(res) {
                        if (res.length == 0) {
                            message('Такого ИНН нет в БД');
                        } else {
                            dadataContainer.html('');
                            $('.dropdown-inn').dropdown();
                            for(var i = 0; i <= res.length; i++)
                            {
                                dadataContainer.append('<div class="dadata-item" data-inn="'+res[i].data.inn+'" data-address="'+res[i].data.address.unrestricted_value+'" data-name=\''+res[i].value +'\'>\
                                <p>'+res[i].value+'</p>\
                                <p><span class="dadata-response-inn">'+res[i].data.inn+'</span> '+res[i].data.address.value+'</p>\
                                </div>\
                                <hr>');
                            }
                        }

                    })
                    .fail(function() {
                        message('Ошибка запроса, попробуйте еще раз');
                    })
        });

        function message(message) {
            $('#inn').siblings('.invalid-feedback').children('strong').html('');
            $('#inn').addClass('is-invalid').parent().append('<span class="invalid-feedback" role="alert">\
                                        <strong>' + message + '</strong>\
                                    </span>');
        }
    })
</script>
@endsection

@section('script-bottom')
@endsection