@extends('backoffice.layouts.vertical')


@section('css')

@endsection

@section('breadcrumb')
<div class="row page-title">
    <div class="col-md-12">
        <nav aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Редактирование данных</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">Редактирование данных</h4>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{ route('backoffice.company.update') }}" enctype="multipart/form-data">
                    <input type="hidden" class="@error('id') is-invalid @enderror" name="id" value="{{ $company->id }}" >
                    @csrf
                    @error('id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn">ИНН</label>
                                <div class="col-lg-10">
                                    <input type="number" class="form-control @error('inn') is-invalid @enderror" id="inn" name="inn" value="{{ $company->inn }}" data-toggle="dropdown">
                                    <div class="dropdown-menu dropdown-inn">
                                        <p class="decription ml-2">Выберите вариант или начните ввод</p>
                                        <div class="dadata">
                                            
                                        </div>
                                    </div>
                                    @error('inn')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn">Название организации</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $company->name }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="address">Юридический адрес</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ $company->address }}">
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="public_name">Публичное название организации</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('public_name') is-invalid @enderror" id="public_name" name="public_name" value="{{ $company->public_name }}">
                                    @error('public_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="actual_address">Фактический адрес</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('actual_address') is-invalid @enderror" id="actual_address" name="actual_address" value="{{ $company->actual_address }}">
                                    @error('actual_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="responsible">Ответственный ФИО, должность</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('responsible') is-invalid @enderror" id="responsible" name="responsible" value="{{$company->responsible }}">
                                    @error('responsible')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="phone">Телефон</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ $company->phone }}">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="email">Эл. почта</label>
                                <div class="col-lg-10">
                                    <input type="text" id="email" name="email" value="{{ $company->email }}" class="form-control form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="industry">Отрасль</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('industry') is-invalid @enderror" id="industry" name="industry" value="{{ $company->industry }}">
                                    @error('industry')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div> -->
                            <div class="mt-4">
                                <h4 class="header-title">Прикрепите файлы</h4>
                                <p class="sub-header">
                                    Все форматы изображений, текстовые word, pdf
                                </p>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn_file">ИНН</label>
                                <div class="col-lg-10">
                                    <input type="file" class="form-control @error('inn_file') is-invalid @enderror" name="inn_file" id="inn_file">
                                    @error('inn_file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="ogrn_file">ОГРН</label>
                                <div class="col-lg-10">
                                    <input type="file" class="form-control @error('ogrn_file') is-invalid @enderror" name="ogrn_file" id="ogrn_file">
                                    @error('ogrn_file')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-primary mb-3">Обновить данные</button>
                            </div>
                        </div>
                    </div>
                </form>

                <!-- <form class="form-horizontal" method="POST" action="{{ route('backoffice.company.update') }}">
                    <input type="hidden" class="@error('id') is-invalid @enderror" name="id" value="{{ $company->id }}">
                    @csrf
                    @error('id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <div class="row">
                        <div class="col-9">
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn">Название</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $company->name }}">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="inn">ИНН</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('inn') is-invalid @enderror" id="inn" name="inn" value="{{ $company->inn }}">
                                    @error('inn')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="address">Юридический адрес</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ $company->address }}">
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="public_name">Публичное название</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('public_name') is-invalid @enderror" id="public_name" name="public_name" value="{{ $company->public_name }}">
                                    @error('public_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="actual_address">Фактический адресе</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('actual_address') is-invalid @enderror" id="actual_address" name="actual_address" value="{{ $company->actual_address }}">
                                    @error('actual_address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="responsible">Ответственный ФИО, должность</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('responsible') is-invalid @enderror" id="responsible" name="responsible" value="{{$company->responsible }}">
                                    @error('responsible')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="phone">Телефон</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ $company->phone }}">
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="email">Эл. почта</label>
                                <div class="col-lg-10">
                                    <input type="text" id="email" name="email" value="{{ $company->email }}" class="form-control form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label" for="industry">Отрасль</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control @error('industry') is-invalid @enderror" id="industry" name="industry" value="{{ $company->industry }}">
                                    @error('industry')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-primary mb-3">Сохранить</button>
                                <button type="button" class="btn btn-outline-danger mb-3" data-toggle="modal" data-target="#deleteModal">Удалить</button>
                            </div>
                        </div>
                    </div>
                </form> -->

            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div><!-- end col -->
</div>
<!-- end row -->
<!-- sample modal content -->
<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Удаление компании</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Вы действительно хотите удалить компанию?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
                <form action="{{ route('backoffice.company.delete') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $company->id }}">
                    <button type="submit" class="btn btn-outline-danger">Удалить</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('script')
<script>
    $(document).ready(function() {
        
        $('.dropdown-inn').on('click', '.dadata-item', function () {
            var inn = $(this).attr('data-inn');
            var address = $(this).attr('data-address');
            var name = $(this).attr('data-name');

            console.log(name);
            $('#inn').val(inn);
            $('#name').val(name);
            $('#address').val(address);
        })

        $('#inn').on('input', function() {
            var query = $(this);
            var dadataContainer = $('.dadata');
            $.post('/backoffice/company/searchDataByInn', {
                        query: query.val()
                    }, function(res) {
                        if (res.length == 0) {
                            message('Такого ИНН нет в БД');
                        } else {
                            dadataContainer.html('');
                            $('.dropdown-inn').dropdown();
                            for(var i = 0; i <= res.length; i++)
                            {
                                dadataContainer.append('<div class="dadata-item" data-inn="'+res[i].data.inn+'" data-address="'+res[i].data.address.unrestricted_value+'" data-name=\''+res[i].value +'\'>\
                                <p>'+res[i].value+'</p>\
                                <p><span class="dadata-response-inn">'+res[i].data.inn+'</span> '+res[i].data.address.value+'</p>\
                                </div>\
                                <hr>');
                            }
                        }

                    })
                    .fail(function() {
                        message('Ошибка запроса, попробуйте еще раз');
                    })
        });

        function message(message) {
            $('#inn').siblings('.invalid-feedback').children('strong').html('');
            $('#inn').addClass('is-invalid').parent().append('<span class="invalid-feedback" role="alert">\
                                        <strong>' + message + '</strong>\
                                    </span>');
        }
    })
</script>
@endsection

@section('script-bottom')
@endsection