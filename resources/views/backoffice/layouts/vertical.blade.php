<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>Medexpert - Медицинский эксперт</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Экспертиза медицинских карт." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="_token" content="{!! csrf_token() !!}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    @if(isset($isDark) && $isDark)
    @include('backoffice.layouts.shared.head', ['isDark' => true])
    @elseif(isset($isRTL) && $isRTL)
    @include('backoffice.layouts.shared.head', ['isRTL' => true])
    @else
    @include('backoffice.layouts.shared.head')
    @endif

</head>

@if(isset($isScrollable) && $isScrollable)

<body class="scrollable-layout">
    
    @elseif(isset($isBoxed) && $isBoxed)

    <body class="left-side-menu-condensed boxed-layout" data-left-keep-condensed="true">
        @elseif(isset($isDarkSidebar) && $isDarkSidebar)

        <body class="left-side-menu-dark">
            @elseif(isset($isCondensedSidebar) && $isCondensedSidebar)

            <body class="left-side-menu-condensed" data-left-keep-condensed="true">
                @else

                <body>
                    @endif

                    @if(isset($withLoader) && $withLoader)
                    <!-- Pre-loader -->
                    <div id="preloader">
                        <div id="status">
                            <div class="spinner">
                                <div class="circle1"></div>
                                <div class="circle2"></div>
                                <div class="circle3"></div>
                            </div>
                        </div>
                    </div>
                    <!-- End Preloader-->
                    @endif

                    <div id="wrapper">
                        @include('backoffice.layouts.shared.header')
                        @include('backoffice.layouts.shared.sidebar')

                        <div class="content-page">
                            <div class="content">
                                <!-- Start Content-->
                                <div class="container-fluid">
                                    @yield('breadcrumb')
                                    @include('flash::message')
                                    @yield('content')
                                </div>
                            </div>

                            @include('backoffice.layouts.shared.footer')
                        </div>
                    </div>

                    @include('backoffice.layouts.shared.rightbar')

                    @include('backoffice.layouts.shared.footer-script')

                    @if (getenv('APP_ENV') === 'local')
                    <script id="__bs_script__">
                        //<![CDATA[
                        // document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.7'><\/script>".replace("HOST", location.hostname));
                        //]]>
                    </script>
                    @endif
                    <script type="text/javascript">
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        });
                    </script>
                    <script>
                        $('#flash-overlay-modal').modal();
                    </script>
                </body>

</html>