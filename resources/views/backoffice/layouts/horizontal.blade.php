<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />

    <title>Medexpert - Медицинский эксперт</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Intechn - Тендерная площадка." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    @include('backoffice.layouts.shared.head')

</head>

<body data-layout="topnav">

    <div id="wrapper">
    @include('flash::message')
        @include('backoffice.layouts.shared.navbar')

        <div class="content-page">
            <div class="content">
                <!-- Start Content-->
                <div class="container-fluid">
                    @yield('breadcrumb')
                    @yield('content')
                </div>
            </div>

            @include('backoffice.layouts.shared.footer')
        </div>
    </div>

    @include('backoffice.layouts.shared.rightbar')

    @include('backoffice.layouts.shared.footer-script')
    <script>
                        $('#flash-overlay-modal').modal();
                    </script>
</body>

</html>