<ul class="metismenu" id="menu-bar">
    <li class="menu-title">Навигация</li>

    <li>
        <a href="{{ route('backoffice.desktop.index') }}" class="{{ (request()->is('/') || request()->is('backoffice/desktop*')) ? 'active' : '' }}">
            <i data-feather="home"></i>
            <!-- <span class="badge badge-success float-right">1</span> -->
            <span>Рабочий стол</span>
        </a>
    </li>
    <li>
        <a href="{{ route('backoffice.expertise.index') }}" class="{{ (request()->is('/expertise') || request()->is('backoffice/expertise*')) ? 'active' : '' }}">
            <i data-feather="check-square"></i>
            <!-- <span class="badge badge-success float-right">1</span> -->
            <span>Экспертиза карт</span>
        </a>
    </li>
    <li>
        <a href="{{ route('backoffice.expert.index') }}" class="{{ (request()->is('/expert') || request()->is('backoffice/expert')) ? 'active' : '' }}">
            <i data-feather="user"></i>
            <!-- <span class="badge badge-success float-right">1</span> -->
            <span>Для эксперта</span>
        </a>
    </li>
    <li>
        <a href="{{ route('backoffice.journal.index') }}" class="{{ (request()->is('/journal') || request()->is('backoffice/journal*')) ? 'active' : '' }}">
            <i data-feather="grid"></i>
            <!-- <span class="badge badge-success float-right">1</span> -->
            <span>Журнал</span>
        </a>
    </li>
    <!-- <li>
        <a href="{{ route('backoffice.analytics.index') }}" class="{{ (request()->is('/analytics') || request()->is('backoffice/analytics*')) ? 'active' : '' }}">
            <i data-feather="activity"></i> -->
            <!-- <span class="badge badge-success float-right">1</span> -->
            <!-- <span>Аналитика</span>
        </a>
    </li> -->
    <li class="menu-title">Технический раздел</li>
    <li>
        <a href="{{ route('backoffice.diagnoses.index') }}" class="{{ (request()->is('/diagnoses') || request()->is('backoffice/diagnoses*')) ? 'active' : '' }}">
            <i data-feather="file-text"></i>
            <!-- <span class="badge badge-success float-right">1</span> -->
            <span>Диагнозы</span>
        </a>
    </li>
    <!-- <li>
        <a href="{{ route('backoffice.similar.index') }}" class="{{ (request()->is('/similar') || request()->is('backoffice/similar*')) ? 'active' : '' }}">
            <i data-feather="file-text"></i> -->
            <!-- <span class="badge badge-success float-right">1</span> -->
           <!-- <span>Критерии</span>
        </a>
    </li> -->
    <li>
        <a href="{{ route('backoffice.docs.index') }}" class="{{ (request()->is('/docs') || request()->is('backoffice/docs*')) ? 'active' : '' }}">
            <i data-feather="file-text"></i>
            <!-- <span class="badge badge-success float-right">1</span> -->
            <span>Документация API</span>
        </a>
    </li>
    <!-- <li class="menu-title">Apps</li> -->
    <!-- @if(Auth::user()->role_id == 1)
    <li>
        <a href="{{ route('backoffice.users.index') }}">
            <i data-feather="users"></i>
            <span> Пользователи </span>
        </a>
    </li>
    @endif

    @if(Auth::user()->active == 1)
    <li>
        <a href="{{ route('backoffice.tender.index') }}">
            <i data-feather="share-2"></i>
            <span> Конкурсы </span>
        </a>
    </li>
    @else
    <li class="isDisabled">
        <a href="#0" class="isDisabled">
            <i data-feather="share-2"></i>
            <span> Конкурсы </span>
        </a>
    </li>
    @endif

    <li>
        <a href="{{ route('backoffice.company.index') }}">
            <i data-feather="monitor"></i>
            <span> Моя компания </span>
        </a>
    </li>
    @if(Auth::user()->role_id == 1)
    <li>
        <a href="{{ route('backoffice.applications.index') }}">
            <i data-feather="grid"></i>
            <span> Все заявки </span>
        </a>
    </li>
    @endif
    @if(Auth::user()->active == 1)
    <li>
        <a href="{{ route('backoffice.application.index') }}">
            <i data-feather="columns"></i>
            <span> Мои заявки </span>
        </a>
    </li>
    @else
    <li class="isDisabled">
        <a href="#0" class="isDisabled">
            <i data-feather="columns"></i>
            <span> Мои заявки </span>
        </a>
    </li>
    @endif -->
    <!-- <li>
        <a href="javascript: void(0);">
            <i data-feather="inbox"></i>
            <span> Email </span>
            <span class="menu-arrow"></span>
        </a>

        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/apps/email/inbox">Inbox</a>
            </li>
            <li>
                <a href="/apps/email/read">Read</a>
            </li>
            <li>
                <a href="/apps/email/compose">Compose</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="briefcase"></i>
            <span> Projects </span>
            <span class="menu-arrow"></span>
        </a>
    
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/apps/project/list">List</a>
            </li>
            <li>
                <a href="/apps/project/detail">Detail</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="bookmark"></i>
            <span> Tasks </span>
            <span class="menu-arrow"></span>
        </a>
    
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/apps/task/list">List</a>
            </li>
            <li>
                <a href="/apps/task/board">Kanban Board</a>
            </li>
        </ul>
    </li>
    <li class="menu-title">Custom</li>
    <li>
        <a href="javascript: void(0);">
            <i data-feather="file-text"></i>
            <span> Pages </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/pages/starter">Starter</a>
            </li>
            <li>
                <a href="/pages/profile">Profile</a>
            </li>
            <li>
                <a href="/pages/activity">Activity</a>
            </li>
            <li>
                <a href="/pages/invoice">Invoice</a>
            </li>
            <li>
                <a href="/pages/pricing">Pricing</a>
            </li>
            <li>
                <a href="/pages/maintenance">Maintenance</a>
            </li>
            <li>
                <a href="/errors/404">Error 404</a>
            </li>
            <li>
                <a href="/errors/500">Error 500</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript: void(0);">
            <i data-feather="layout"></i>
            <span> Layouts </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/layout-example/horizontal">Horizontal Nav</a>
            </li>
            <li>
                <a href="/layout-example/rtl">RTL</a>
            </li>
            <li>
                <a href="/layout-example//dark">Dark</a>
            </li>
            <li>
                <a href="/layout-example/scrollable">Scrollable</a>
            </li>
            <li>
                <a href="/layout-example/boxed">Boxed</a>
            </li>
            <li>
                <a href="/layout-example/loader">With Pre-loader</a>
            </li>
            <li>
                <a href="/layout-example/dark-sidebar">Dark Side Nav</a>
            </li>
            <li>
                <a href="/layout-example/condensed-sidebar">Condensed Nav</a>
            </li>
        </ul>
    </li>

    <li class="menu-title">Components</li>

    <li>
        <a href="javascript: void(0);">
            <i data-feather="package"></i>
            <span> UI Elements </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/ui/bootstrap">Bootstrap UI</a>
            </li>
            <li>
                <a href="javascript: void(0);" aria-expanded="false">Icons
                    <span class="menu-arrow"></span>
                </a>
                <ul class="nav-third-level" aria-expanded="false">
                    <li>
                        <a href="/ui/icons-feather">Feather Icons</a>
                    </li>
                    <li>
                        <a href="/ui/icons-unicons">Unicons Icons</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/ui/widgets">Widgets</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="javascript: void(0);" aria-expanded="false">
            <i data-feather="file-text"></i>
            <span> Forms </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/forms/basic">Basic Elements</a>
            </li>
            <li>
                <a href="/forms/advanced">Advanced</a>
            </li>
            <li>
                <a href="/forms/validation">Validation</a>
            </li>
            <li>
                <a href="/forms/wizard">Wizard</a>
            </li>
            <li>
                <a href="/forms/editor">Editor</a>
            </li>
            <li>
                <a href="/forms/fileupload">File Uploads</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="/charts" aria-expanded="false">
            <i data-feather="pie-chart"></i>
            <span> Charts </span>
        </a>
    </li>

    <li>
        <a href="javascript: void(0);" aria-expanded="false">
            <i data-feather="grid"></i>
            <span> Tables </span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level" aria-expanded="false">
            <li>
                <a href="/table/basic">Basic</a>
            </li>
            <li>
                <a href="/table/datatables">Advanced</a>
            </li>
        </ul>
    </li> -->
</ul>