<?php

return [

    'register' => [
        'title' => [
            'foruser' => 'Вот это удача! Добро пожаловать в Flylunch',
            'forus'   => 'Зарегистрировано новое производство'
        ]
    ],

    'passwords' => [
        'reset'    => 'Сброс пароля на Flylunch.ru',
        'reminder' => 'Список зарегистрированных доменов'
    ]
];