<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы напоминания пароля
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки возвращаются брокером паролей на неудачные
    | попытки обновления пароля в таких случаях, как ошибочный код сброса
    | пароля или неверный новый пароль.
    |
    */

    'reset'     => 'Ваш пароль был сброшен!',
    'sent'      => 'Ссылка на сброс пароля была отправлена!',
    'throttled' => 'Пожалуйста, подождите перед повторной попыткой.',
    'token'     => 'Ошибочный код сброса пароля.',
    'user'      => 'Не удалось найти пользователя с указанным электронным адресом.',

    'listdomain' => 'На электронный адрес :email отправлено письмо с 
                     инструкциями по восстановлению доступа к вашим аккаунтам.<br><br>
                     Если Вы не получили письмо, нажмите кнопку «Отправить повторно» 
                     или обратитесь в техподдержку. <br><br>
                     Если Вы неправильно указали почту при регистрации аккаунта, 
                     зарегистрируйтесь заново или сформируйте запрос в техподдержку 
                     по номеру телефона +7 812 467-38-29"'
];
