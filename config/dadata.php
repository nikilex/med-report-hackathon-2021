<?php

return [
    'api' => [
        'key'    => env('DADATA_KEY'),
        'secret' => env('DADATA_SECRET')
    ]
    ];