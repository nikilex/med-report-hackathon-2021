<?php

return [
    'doc'  => array('application/msword', 'application/vnd.ms-office'),
    'docx' => array('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/zip'),
    'html' => array('text/html', 'text/html'),
    'htm'  => array('text/html', 'text/html'),
];