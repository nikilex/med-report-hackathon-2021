<?php

return [
    'commercial_proposal'   => 'public/commercial_proposal',
    'menu_file'             => 'public/menu_file',
    'assortiment_menu_file' => 'public/assortiment_menu_file',
    'photo_set_lunch'       => 'public/photo_set_lunch',
    'inn_file'              => 'public/inn_file',
    'ogrn_file'             => 'public/ogrn_file',
];