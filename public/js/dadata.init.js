$(document).ready(function() {
    function message(message) {
        $('#indastry_id').siblings('.invalid-feedback').children('strong').html('');
        $('#indastry_id').addClass('is-invalid').parent().append('<span class="invalid-feedback" role="alert">\
                                    <strong>' + message + '</strong>\
                                </span>');
    }

    function errClear() {
        message('');
        $('#indastry_id').siblings('.invalid-feedback').remove();
        $('#indastry_id').removeClass('is-invalid')

    }
})

$(document).ready(function() {
    //$('#step4').tab('show');
    $('.dropdown-inn').on('click', '.dadata-item', function() {
        var inn = $(this).attr('data-inn');
        var address = $(this).attr('data-address');
        var name = $(this).attr('data-name');
        var step = $(this).closest('.step');

        console.log('yep');
        step.find('.inn').val(inn);
        step.find('.name').val(name);
        step.find('.address').val(address);
    })

    $('.companies').on('input', '.inn', function() {
        var query = $(this);
        var dadataContainer = $(this).siblings('.dropdown-inn').find('.dadata');
        $.post('/backoffice/company/searchDataByInn', {
                query: query.val()
            }, function(res) {
                if (res.length == 0) {
                    message('Такого ИНН нет в БД');
                } else {
                    dadataContainer.html('');
                    $('.dropdown-inn').dropdown();
                    for (var i = 0; i <= res.length; i++) {
                        dadataContainer.append('<div class="dadata-item" data-inn="' + res[i].data.inn + '" data-address="' + res[i].data.address.unrestricted_value + '" data-name=\'' + res[i].value + '\'>\
                            <p>' + res[i].value + '</p>\
                            <p><span class="dadata-response-inn">' + res[i].data.inn + '</span> ' + res[i].data.address.value + '</p>\
                            </div>\
                            <hr>');
                    }
                }

            })
            .fail(function() {
                message('Ошибка запроса, попробуйте еще раз');
            })
    });

    function message(message) {
        $('#inn').siblings('.invalid-feedback').children('strong').html('');
        $('#inn').addClass('is-invalid').parent().append('<span class="invalid-feedback" role="alert">\
                                    <strong>' + message + '</strong>\
                                </span>');
    }
})

$(document).ready(function() {
    function msgCompany(element, message) {
        $(element).siblings('.invalid-feedback').children('strong').html('');
        $(element).addClass('is-invalid').parent().append('<span class="invalid-feedback" role="alert">\
                                    <strong>' + message + '</strong>\
                                </span>');
    }

    function errClearCompany(element) {
        msgCompany(element, '');
        $(element).siblings('.invalid-feedback').remove();
        $(element).removeClass('is-invalid')

    }

    $('.addCompany').on('click', function() {
        var container = $(this).closest('.companies');
        container.find('.buttons').before($('<label class="col-lg-4 col-form-label mt-3">ИНН</label>\
                                <div class="col-lg-10 step">\
                                    <div class="row">\
                                        <div class="col-lg-12">\
                                            <div class="row">\
                                                <div class="col-9">\
                                                    <input autocomplete="off" type="text" class="form-control inn" name="companies[]" data-toggle="dropdown">\
                                                    <div class="dropdown-menu dropdown-inn">\
                                                            <p class="decription ml-2">Выберите вариант или начните ввод</p>\
                                                            <div class="dadata">\
                                                            </div>\
                                                        </div>\
                                                </div>\
                                                <div class="col-3">\
                                                    <button type="button" class="btn btn-outline-info  sendInnButton">\
                                                        <div class="spinner-border text-dark d-none" style="width: 20px; height: 20px" role="status">\
                                                            <span class="sr-only">Loading...</span>\
                                                        </div>\
                                                        Заполнить по ИНН\
                                                    </button>\
                                                    <button type="button" class="btn btn-outline-danger d-none rewrite">Заполнить заново</button>\
                                                </div>\
                                            </div>\
                                            <label class="" for="name">Название компании</label>\
                                            <input autocomplete="off" type="text" class="form-control name" name="name[]">\
                                            <label class="" for="name">Адрес компании</label>\
                                            <input autocomplete="off" type="text" class="form-control address" name="address[]">\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="w-100"></div>'));
    })

    $('.companies').on('click', '.rewrite', function() {
        var stepContainer = $(this).closest('.step');
        stepContainer.find('.inn').val('').attr('readonly', false);
        stepContainer.find('.name').val('').attr('readonly', false);
        stepContainer.find('.address').val('').attr('readonly', false);
        $(this).addClass('d-none');
        stepContainer.find('.sendInnButton').removeClass('d-none');
    });

    $('.companies').on('click', '.sendInnButton', function() {
        var stepContainer = $(this).closest('.step');
        var query = stepContainer.find('.inn');
        var button = $(this);
        errClearCompany(stepContainer.find('.inn'));
        if (query.val().trim() != '') {
            button.children('.spinner-border').removeClass('d-none').attr('disabled', true);
            $.post('/backoffice/company/data', {
                    query: query.val()
                }, function(res) {
                    button.attr('readonly', false);
                    button.children('.spinner-border').addClass('d-none');
                    if (res.length == 0) {
                        msgCompany(stepContainer.find('.inn'), 'Такого ИНН нет в БД');
                    } else {
                        query.attr('readonly', true);
                        stepContainer.find('.name').val(res[0].value).attr('readonly', true);
                        stepContainer.find('.address').val(res[0].data.address.unrestricted_value).attr('readonly', true);
                        stepContainer.find('.rewrite').removeClass('d-none');
                        stepContainer.find('.sendInnButton').addClass('d-none')
                    }

                })
                .fail(function() {
                    stepContainer.find('.sendInnButton').children('.spinner-border').addClass('d-none');
                    stepContainer.find('.sendInnButton').attr('disabled', false);
                    msgCompany(stepContainer.find('.inn'), 'Ошибка запроса, попробуйте еще раз');
                })
        } else {
            msgCompany(stepContainer.find('.inn'), 'ИНН должен быть заполнен, длина строки должна равняться 10');
        }
    })
})