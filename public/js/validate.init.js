$.validator.addMethod("equalToValue", function(value, element, param) { 
    return this.optional(element) || value === param; 
}, "Нет открытых конкурсов");

$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (( element.files[0].size / 1024 / 1024) <= param)
}, 'Размер файла не может превышать {0} Мб');


$('.next').on('click', function() {
    var formApp = $("#formApp");
    formApp.validate({
        focusInvalid: false,
        lang: "ru",
        rules: {
            indastry_id: {
                required: true,
                equalToValue: '4'
            },
            commercial_proposal_file: {
                required: true,
                filesize: 10,
            },
            menu_file: {
                required: true,
                filesize: 10,
            },
            photo_set_lunch: {
                required: true,
                filesize: 10,
            },
            taxation_system_id: {
                required: true,
            },
            number_employees: {
                required: true,
            },
            number_branches: {
                required: true,
            },
            experience: {
                required: true,
            },
            client_id: {
                required: true,
            }
        },
        messages: {
            // indastry_id: {
            //     required: "Выберите конкурс",
            // },
            commercial_proposal_file: {
                required: "Загрузите файл"
            },
            menu_file: {
                required: "Загрузите файл"
            },
            photo_set_lunch: {
                required: "Загрузите файл"
            },
        },
    });
    if (formApp.valid() == true){
        if ($('#step1').is(":visible")){
            current_fs = $('#step1');
            next_fs = $('#step2');
        }

        if ($('#step2').is(":visible"))
        {
            current_fs = $('#step2');
            next_fs = $('#step3');
        }

        if ($('#step3').is(":visible"))
        {
            current_fs = $('#step3');
            next_fs = $('#step4');
        }
        
        next_fs.tab('show');
        current_fs.hide();
    }
})