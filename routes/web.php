<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect(route('backoffice.desktop.index'));
// })->middleware('auth');

Route::get('/', 'LandingController@index');
Route::post('/backoffice/getCriteria', 'Backoffice\IndexController@getCriteria');


Route::as('backoffice.')
    ->prefix('/backoffice')
    ->middleware(['auth'])
    ->namespace('Backoffice')
    ->group(function () {

        Route::post('/export', 'IndexController@export')->name('export');

        Route::get('/noaccess', function () {
            return view('backoffice.noaccess');
        })->name('noaccess');

        Route::get('/', 'IndexController@index')->name('index');
        Route::get('/download', 'IndexController@download')->name('download');

        //Рабочий стол
        Route::as('desktop.')
            ->prefix('/desktop')
            ->namespace('Desktop')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                // Route::get('/parcer', 'IndexController@parcer')->name('parcer');
                // Route::post('/parcer', 'IndexController@parcer')->name('parcer');
                // Route::post('/expert', 'IndexController@expert')->name('expert');
                // Route::get('/expert/{id}', 'IndexController@view')->name('expert.view');
                // Route::get('/download-file', 'IndexController@downloadFile')->name('download-file');
            });

        //Экспертиза
        Route::as('expertise.')
            ->prefix('/expertise')
            ->namespace('Expertise')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/parcer', 'IndexController@parcer')->name('parcer');
                Route::post('/parcer', 'IndexController@parcer')->name('parcer');
                Route::post('/expert', 'IndexController@expert')->name('expert');
                Route::get('/expert/{id}', 'IndexController@view')->name('expert.view');
                Route::get('/download-file', 'IndexController@downloadFile')->name('download-file');
            });

        //Для эксперта
        Route::as('expert.')
            ->prefix('/expert')
            ->namespace('Expert')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
            });

        //Журнал
        Route::as('journal.')
            ->prefix('/journal')
            ->namespace('Journal')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
            });

        //Аналитика
        Route::as('analytics.')
            ->prefix('/analytics')
            ->namespace('Analytics')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
            });

        //Диагнозы
        Route::as('diagnoses.')
            ->prefix('/diagnoses')
            ->namespace('Diagnoses')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/{id}', 'IndexController@edit')->name('edit');
                Route::post('/store', 'IndexController@store')->name('store');
                Route::post('/update', 'IndexController@update')->name('update');
                Route::post('/delete', 'IndexController@delete')->name('delete');

                Route::get('/{id}/criterias', 'IndexController@criterias')->name('criterias');
                Route::get('/{id}/examinations', 'IndexController@examinations')->name('examinations');
                Route::get('/{id}/treatments', 'IndexController@treatments')->name('treatments');

                Route::get('/similar/{id}', 'IndexController@similar')->name('similar');
                
                
            });

        // Обследования/Лечение
        Route::as('examinations.')
            ->prefix('/examinations')
            ->namespace('Examinations')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/{id}', 'IndexController@edit')->name('edit');
                Route::post('/store', 'IndexController@store')->name('store');
                Route::post('/update', 'IndexController@update')->name('update');
                Route::post('/delete', 'IndexController@delete')->name('delete');     
            });


        //Документация API
        Route::as('docs.')
            ->prefix('/docs')
            ->namespace('Docs')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
            });

        //Аналитика
        Route::as('similar.')
            ->prefix('/similar')
            ->namespace('Similar')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::post('/getExaminations', 'IndexController@getExaminations');
                Route::post('/saveExam', 'IndexController@saveExam');
            });

        // Управление пользователями
        Route::as('users.')
            ->prefix('/users')
            ->namespace('Users')
            ->middleware(['isSuperadmin'])
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/add', 'IndexController@add')->name('add');
                Route::get('/edit/{id}', 'IndexController@edit')->name('edit');
                Route::get('/edit/{user_id}/application/{application_id}', 'IndexController@application')->name('application');
                Route::post('/store', 'IndexController@store')->name('store');
                Route::post('/update', 'IndexController@update')->name('update');
                Route::post('/delete', 'IndexController@delete')->name('delete');
            });

        // Раздел компании
        Route::as('company.')
            ->prefix('/company')
            ->namespace('Company')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/edit', 'IndexController@edit')->name('edit');
                Route::post('/store', 'IndexController@store')->name('store');
                Route::post('/update', 'IndexController@update')->name('update');
                Route::post('/delete', 'IndexController@delete')->name('delete');
                Route::post('/data', 'IndexController@getDataByInn')->name('data');
                Route::post('/searchDataByInn', 'IndexController@searchDataByInn')->name('searchDataByInn');
            });

        // Раздел мои заявки
        Route::as('application.')
            ->prefix('/application')
            ->namespace('Application')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/add', 'IndexController@add')->name('add');
                Route::get('/edit/{id}', 'IndexController@edit')->name('edit');
                Route::post('/store', 'IndexController@store')->name('store');
                Route::post('/update', 'IndexController@update')->name('update');
                Route::post('/delete', 'IndexController@delete')->name('delete');
                Route::post('/status', 'IndexController@status')->name('status');
            });

        // Раздел все заявки
        Route::as('applications.')
            ->prefix('/applications')
            ->namespace('Applications')
            ->middleware(['isSuperadmin'])
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/application/{id}', 'IndexController@application')->name('application');
            });

        //Раздел конкурсы
        Route::as('tender.')
            ->prefix('/tender')
            ->namespace('Tender')
            ->group(function () {
                Route::get('/', 'IndexController@index')->name('index');
            });
    });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
