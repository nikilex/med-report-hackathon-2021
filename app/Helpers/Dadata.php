<?php

namespace App\Helpers;

use Dadata\DadataClient;

class Dadata {

    /**
     * @var
     */
    protected $dadata;

    public function __construct()
    {
        $token  = config('dadata.api.key');
        $secret = config('dadata.api.secret');
        $this->dadata = new DadataClient($token, $secret);
    }

    /**
     * Получает данные организации по ее ИНН
     * через API https://dadata.ru/
     * @param  string $nameParametr
     * @param  string $query
     * @param  int    $count
     * @param  array  $kwargs
     * @return mixed
     */
    public function findById(
        string $nameParametr, 
        string $query, 
        int $count = 1, 
        array $kwargs = []
    ) {
        return $this->dadata ->findById($nameParametr, $query, $count, $kwargs);
    }

    /**
     * Получает массив подсказок организаций по ИНН
     * через API https://dadata.ru/
     * @param  string $nameParametr
     * @param  string $query
     * @param  int    $count
     * @param  array  $kwargs
     * @return array
     */
    public function suggest(
        string $nameParametr, 
        string $query, 
        int $count = 1, 
        array $kwargs = []
    ) {
        return $this->dadata->suggest($nameParametr, $query, $count, $kwargs);
    }
}
