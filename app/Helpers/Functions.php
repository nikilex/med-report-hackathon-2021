<?php

namespace App\Helpers;

class Functions
{
    /**
     * Получает данные организации по ее ИНН
     * через API https://dadata.ru/
     * @param  string $nameParametr
     * @param  string $query
     * @param  int    $count
     * @param  array  $kwargs
     * @return mixed
     */
    public static function dadata(
        string $nameParametr, 
        string $query, 
        int $count = 1, 
        array $kwargs = []
    ) {
        $token  = config('dadata.api.key');
        $secret = config('dadata.api.secret');
        $dadata = new \Dadata\DadataClient($token, '263a8d67522b69470c437966515fe08d6e0706ae');
        return $dadata->findById($nameParametr, $query, $count, $kwargs);
    }
}