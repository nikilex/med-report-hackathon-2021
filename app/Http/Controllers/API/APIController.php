<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\CriteriaService;
use App\Services\DiagnosisService;
use App\Services\ExpertService;
use Illuminate\Http\Request;

class APIController extends Controller
{
    protected DiagnosisService $diagnosisService;

    protected ExpertService $expertService;

    protected CriteriaService $criteriaService;

    public function __construct(
        DiagnosisService $diagnosisService,
        ExpertService $expertService,
        CriteriaService $criteriaService
    )   {
        $this->diagnosisService = $diagnosisService;
        $this->expertService    = $expertService;
        $this->criteriaService  = $criteriaService;
    }

    /**
     * Возвращает все диагнозы из бд
     * 
     * @return JSON
     */
    public function getDiagnoses()
    {
        $diagnosis = $this->diagnosisService->getAllDiagnoses();
        return response()->json($diagnosis);
    }

    /**
     * Проводит экспертизу карточки пациента
     * 
     * @param  Illuminate\Http\Request;
     * @return JSON
     */
    public function expert(Request $request)
    {
        /** 
         * {
         *  age: 25,
         *  diagnos: J01,
         *  separator: ','
         *  criterias: 'Критерий 1, Критерий 2',
         *  examinations: 'Обследование 1, Обследование 2',
         *  treatment: 'Лечение 1, Лечение 2'
         * }
         */
        $expert      = $this->expertService->expert($request->toArray());
        $criterias   = $this->criteriaService->getCriteriasByIds($expert['criteria']);
        $expertModel = $this->expertService->store($expert);

        $expertModel['error_criteria'] = $criterias;
        
        return response()->json($expertModel);
    }
}
