<?php

namespace App\Http\Controllers\Backoffice\Users;

use App\Http\Controllers\Backoffice\Users\Requests\UserDeleteRequest;
use App\Http\Controllers\Backoffice\Users\Requests\UserStoreRequest;
use App\Http\Controllers\Backoffice\Users\Requests\UserUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\City;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class IndexController extends Controller
{
    /**
     * Отображение всех пользователей
     * @return View
     */
    public function index(Request $request)
    {
        $users = User::get();
        
        return view('backoffice.users.index', compact('users'));
    }

    /**
     * Отображение добавления пользователя
     * @return View
     */
    public function add()
    {
        return view('backoffice.users.add', compact('cities', 'roles'));
    }

    /**
     * Отображение редактирования пользователя
     * @param  int $id
     * @return View
     */
    public function edit(int $id)
    {
        $user = User::find($id);

        return view('backoffice.users.edit', compact('user'));
    }

    /**
     * Создание пользователя
     * @param  UserStoreRequest
     * @return Redirect
     */
    public function store(UserStoreRequest $request)
    {
        $user = User::create([
            'name'          => $request->name,
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
        ]);
        return redirect()->route('backoffice.users.index')->with('success', 'Пользователь успешно создан');
    }

    /**
     * Обновление пользователя
     * @param  UserUpdateaRequest
     * @return Redirect
     */
    public function update(UserUpdateRequest $request)
    {
        $user = User::find($request->id)->update([
            'name'          => $request->name,
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
        ]);
        return redirect()->route('backoffice.users.index')->with('success', 'Данные пользователя успешно обновлены');
    }

    /**
     * Удаление пользователя
     * @param  UserDeleteRequest
     * @return Redirect
     */
    public function delete(UserDeleteRequest $request)
    {
        $user = User::find($request->id)->delete();
        return redirect()->route('backoffice.users.index')->with('success', 'Пользователь успешно удален');
    }
}
