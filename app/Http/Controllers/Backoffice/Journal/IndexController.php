<?php

namespace App\Http\Controllers\Backoffice\Journal;

use App\Http\Controllers\Controller;
use App\Models\Expert;
use App\Services\ExpertService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var ExpertService
     */
    protected ExpertService $expertService;

    public function __construct(
        ExpertService $expertService
    )   {
        $this->expertService = $expertService;
    }

    public function index()
    {
        $experts = $this->expertService->getPaginate();
        return view('backoffice.journal.index', compact('experts'));
    }
}
