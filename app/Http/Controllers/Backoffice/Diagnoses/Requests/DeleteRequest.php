<?php

namespace App\Http\Controllers\Backoffice\Diagnoses\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Laracasts\Flash\Flash;

class DeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'diagnosId' => 'required',
        ];
    }

    public function messages()
    {
        return [
            
        ];
    }

    /**
     * Handle a failed validation attempt.
     */
    protected function failedValidation(Validator $validator)
    {
        Flash::error("Необходимо заполнить обязательные поля!");
        return parent::failedValidation($validator);
    } 
}
