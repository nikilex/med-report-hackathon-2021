<?php

namespace App\Http\Controllers\Backoffice\Diagnoses;

use App\Http\Controllers\Backoffice\Diagnoses\Requests\DeleteRequest;
use App\Http\Controllers\Backoffice\Diagnoses\Requests\StoreRequest;
use App\Http\Controllers\Backoffice\Diagnoses\Requests\UpdateRequest;
use App\Http\Controllers\Backoffice\Users\Requests\UserUpdateRequest;
use App\Http\Controllers\Controller;
use App\Services\DiagnosisService;
use App\Services\ExaminationService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var DiagnosisService
     */
    protected DiagnosisService $diagnosisService;

    /**
     * @var ExaminationService
     */
    protected ExaminationService $examinationService;

    public function __construct(
        DiagnosisService   $diagnosisService,
        ExaminationService $examinationService
    )   {
        $this->diagnosisService   = $diagnosisService;
        $this->examinationService = $examinationService;
    }

    public function index()
    {
        $diagnoses = $this->diagnosisService->getAllPaginate(5);
        return view('backoffice.diagnoses.index', compact('diagnoses'));
    }

    public function criterias($id)
    {
        $diagnos      = $this->diagnosisService->getById($id);
        $examinations = $this->examinationService->getByDiagnosIdWithoutSimilar($id, 'examination');
        $treatments   = $this->examinationService->getByDiagnosIdWithoutSimilar($id, 'treatment');
        return view('backoffice.diagnoses.criterias', compact('diagnos', 'examinations', 'treatments'));
    }

    public function treatments($id)
    {
        $diagnos      = $this->diagnosisService->getById($id);
        $examinations   = $this->examinationService->getByDiagnosIdWithoutSimilar($id, 'treatment');
        return view('backoffice.diagnoses.examinations', compact('diagnos', 'examinations'));
    }

    public function examinations($id)
    {
        $diagnos      = $this->diagnosisService->getById($id);
        $examinations = $this->examinationService->getByDiagnosIdWithoutSimilar($id, 'examination');
        return view('backoffice.diagnoses.examinations', compact('diagnos', 'examinations'));
    }

    public function similar($id)
    {
        $exam    = $this->examinationService->getById($id);
        $similar = $this->examinationService->getSimilar($id);
        return view('backoffice.diagnoses.similar', compact('exam', 'similar'));
    }

    public function edit($id)
    {
        $diagnos = $this->diagnosisService->getById($id);
        return view('backoffice.diagnoses.edit', compact('diagnos'));
    }

    public function store(StoreRequest $request)
    {
        $diagnos = $this->diagnosisService->store($request->toArray());
        flash('Запись успешно добавлена!')->success();
        return back();
    }

    public function update(UpdateRequest $request)
    {
        $diagnos = $this->diagnosisService->update($request->toArray());
        flash('Запись успешно обновлена!')->success();
        return redirect(route('backoffice.diagnoses.index'));
    }

    public function delete(DeleteRequest $request)
    {
        $diagnos = $this->diagnosisService->delete($request->toArray());
        flash('Запись успешно удалена!')->success();
        return redirect(route('backoffice.diagnoses.index'));
    }
}
