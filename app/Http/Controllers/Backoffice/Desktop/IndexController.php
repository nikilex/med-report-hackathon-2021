<?php

namespace App\Http\Controllers\Backoffice\Desktop;

use App\Http\Controllers\Backoffice\Desktop\Requests\IndexRequest;
use App\Http\Controllers\Controller;
use App\Models\Criteria;
use App\Models\Examination;
use App\Models\Expert;
use App\Services\CriteriaService;
use App\Services\DiagnosisService;
use App\Services\ExpertService;
use App\Services\ParcerService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var ExpertService
     */
    protected ExpertService $expertService;

    public function __construct(
        ExpertService $expertService
    ) {
        $this->expertService = $expertService;
    }

    public function index()
    {       
        $expertService = $this->expertService->getAll();

        $count = [
            'all'          => $expertService->count(),
            'allError'     => 0,
            'criteria'     => 0,
            'examinations' => 0,
            'treatment'    => 0,
            'dynamic'      => [],
            'noyabr'       => 0,
            'urengoy'      => 0,
        ];

        foreach($expertService as $expert) {
            $count['criteria'] = $count['criteria'] + $expert->count_error_criteria;
            $count['examinations'] = $count['examinations'] + $expert->count_error_examination;
            $count['treatment'] = $count['treatment'] + $expert->count_error_treatment;
            array_push($count['dynamic'], $count['criteria'] + $count['examinations'] + $count['treatment']);
           

            // Новоуренгойская ЦГБ
            switch($expert->hospital) {
                case 'Ноябрьская ЦГБ':
                    $count['noyabr'] = $count['criteria'] + $count['examinations'] + $count['treatment'];
                    break;
                case 'Новоуренгойская ЦГБ':
                    $count['urengoy'] = $count['criteria'] + $count['examinations'] + $count['treatment'];
                break;
            }
        }

        $count['allError'] =  $count['allError'] + $count['criteria'] + $count['examinations'] + $count['treatment'];
        
        $count['noyabr']  = ($count['noyabr'] * 100 ) / ($count['noyabr'] + $count['urengoy']);
        $count['urengoy'] = ($count['urengoy'] * 100 ) / ($count['noyabr'] + $count['urengoy']);

        return view('backoffice.desktop.index', compact('expertService', 'count'));
    }

}
