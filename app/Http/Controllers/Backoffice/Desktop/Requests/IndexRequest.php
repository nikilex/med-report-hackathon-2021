<?php

namespace App\Http\Controllers\Backoffice\Desktop\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'htmlFile' => 'required|max:10000|mimes:html,htm',
        ];
    }



    public function messages()
    {
        return [
            'htmlFile.required' => 'Вы не выбрали файл',
        ];
    }
}
