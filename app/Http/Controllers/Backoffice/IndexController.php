<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use App\Services\DiagnosisService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use niklasravnsborg\LaravelPdf\Facades\Pdf as FacadesPdf;
use niklasravnsborg\LaravelPdf\Pdf as LaravelPdfPdf;
use PDF;

class IndexController extends Controller
{
    /**
     * @var DiagnosisService
     */
    protected DiagnosisService $diagnosisService;

    public function __construct(
        DiagnosisService $diagnosisService
    ) {
        $this->diagnosisService = $diagnosisService;
    }

    public function index()
    {
        return route('backoffice.users.index');
    }

    /**
     * Экспортирует экспертизу в печатный вариант
     * 
     * @param Illuminate\Http\Request
     */
    public function export(Request $request)
    {
        // $pdf = App::make('dompdf.wrapper');
        // $pdf->loadHTML($request->export_data);
        // return $pdf->stream();

        $header = '<h1>Экспертиза № 55</h1> <h2>Завершена в июне 2021 года</h2> <p>ГБУЗ ЯНАО</p> <p>Ноябрьская ЦГБ</p><br><br>';
        $footer = '<br><br><br>Главный аналитик ГБУЗ ЯНАО "Ноябрьская ЦГБ"<br> <h4>Сиротина Татьяна Владимировна</h4>';

        $pdf = FacadesPdf::loadHTML($header . $request->export_data . $footer);
	    return $pdf->stream('document.pdf');
    }

    /**
     * Возвращает критерии диагноза
     */
    public function getCriteria(Request $request)
    {
        $criteria = $this->diagnosisService->getDiagnosCriteria($request->diagnos_id);
        return view('parts.criteria', compact('criteria'))->render();
    }

    /**
     * Загрузка файлов
     * @param  string $request
     * @return Storage
     */
    public function download(Request $request)
    {
        return Storage::download($request->file);
    }
}
