<?php

namespace App\Http\Controllers\Backoffice\Similar;

use App\Http\Controllers\Controller;
use App\Models\Diagnosis;
use App\Services\DiagnosisService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var DiagnosisService
     */
    protected DiagnosisService $diagnosisService;

    public function __construct(
        DiagnosisService $diagnosisService
    ) {
        $this->diagnosisService = $diagnosisService;
    }

    public function index()
    {
        $diagnosis = $this->diagnosisService->getAllDiagnoses();

        return view('backoffice.similar.index', compact('diagnosis'));
    }

    public function getExaminations(Request $request)
    {
        $id = $request->diagnos_id;
        $examinations = $this->diagnosisService->getByIdWithExaminationsWithoutSimilar($id);

        return $examinations;
    }

    public function saveExam(Request $request)
    {
        Diagnosis::where('id', $request->diagnos_id)->first()->examinations()->create([
            'name'         => $request->new_exam,
            'type'         => $request->type,
            'examinations_similar_id' => $request->diagnos_exam_id,
        ]);
    }
}
