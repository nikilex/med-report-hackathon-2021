<?php

namespace App\Http\Controllers\Backoffice\Expert;

use App\Http\Controllers\Controller;
use App\Services\DiagnosisService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
        /**
     * @var DiagnosisService
     */
    protected DiagnosisService $diagnosisService;

    public function __construct(
        DiagnosisService $diagnosisService
    ) {
        $this->diagnosisService = $diagnosisService;
    }

    public function index()
    {
        $diagnosis = $this->diagnosisService->getAllDiagnoses();

        return view('backoffice.expert.view', compact('diagnosis'));
    }
}
