<?php

namespace App\Http\Controllers\Backoffice\Expertise;

use App\Http\Controllers\Backoffice\Expertise\Requests\ExpertiseRequest;
use App\Http\Controllers\Controller;
use App\Models\Criteria;
use App\Models\Examination;
use App\Models\Expert;
use App\Services\CriteriaService;
use App\Services\DiagnosisService;
use App\Services\ExpertService;
use App\Services\ParcerService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
        /**
     * @var DiagnosisService
     */
    protected DiagnosisService $diagnosisService;

    /**
     * @var ParcerService $parcerService
     */
    protected ParcerService $parcerService;

    protected ExpertService $expertService;

    protected CriteriaService $criteriaService;

    public function __construct(
        DiagnosisService $diagnosisService,
        ParcerService    $parcerService,
        ExpertService    $expertService,
        CriteriaService  $criteriaService
    ) {
        $this->diagnosisService = $diagnosisService;
        $this->parcerService    = $parcerService;
        $this->expertService    = $expertService;
        $this->criteriaService  = $criteriaService;
    }

    public function index()
    {
        // $str1 = 'ПЦР мазок из зева к микоплазме пневмо и хламидии пневмо';
        // $str2 = 'мазок из зева к микоплазме пневмо и хламидии пневмо';
        // dd(similar_text($str1, $str2, $perc), $perc);
      //  $diagnosis = $this->diagnosisService->getAllDiagnoses();
        
        return view('backoffice.expertise.index');
    }

    public function downloadFile()
    {
        return response()->download( public_path('card.html') );
    }

    public function expert(Request $request)
    {
        $expert      = $this->expertService->expert($request->toArray());
        $criterias   = $this->criteriaService->getCriteriasByIds($expert['criteria']);
        $expertModel = $this->expertService->store($expert);
        
        return view('backoffice.expert.index', compact('expert', 'criterias'));
    }

    public function view($id)
    {
        $expertModel  = Expert::find($id);
        $criteriasIds = explode(',', $expertModel->error_criteria);
        $criterias    = $this->criteriaService->getCriteriasByIds($criteriasIds);
        $expert = [
            'id' => $expertModel->id,
            'examinations' => explode(',', $expertModel->error_examination),
            'treatment'   => explode(',', $expertModel->error_treatment),
        ];

        return view('backoffice.expert.index', compact('expert', 'criterias'));
    }

    public function parcer(ExpertiseRequest $request, ParcerService $parcerService)
    {

        if ($request->htmlFile) {
            $card = $parcerService->parceHtml($request->htmlFile->path());
            if (isset($card['diagnoses'])) {
                $diagnosis = $this->diagnosisService->getByCodeWithExaminations($card['diagnoses']['code']);
                $checkedCard = $this->parcerService->checkCard($card);
            } else {
                return back()->with('error', 'Неверный формат карточки');
            }
            // dd($checkedCard);
            return view('backoffice.expertise.index', compact('card', 'checkedCard'));
        }

        return view('backoffice.expertise.index');
    }
}
