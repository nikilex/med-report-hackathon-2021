<?php

namespace App\Http\Controllers\Backoffice\Examinations;

use App\Http\Controllers\Controller;
use App\Services\ExaminationService;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var ExaminationService
     */
    protected ExaminationService $examinationService;

    public function __construct(
        ExaminationService $examinationService
    )   {
        $this->examinationService = $examinationService;
    }

    public function store(Request $request)
    {
        $this->examinationService->store($request->toArray());
        flash('Запись успешно добавлена')->success();
        return back();
    }

    public function update(Request $request)
    {
        $this->examinationService->update($request->examId, $request->toArray());
        flash('Запись успешно добавлена')->success();
        return back();
    }

    public function edit($id)
    {
        $examination = $this->examinationService->getById($id);
        $similar     = $this->examinationService->getSimilar($id);

        return view('backoffice.diagnoses.examinations_edit', compact('examination', 'similar'));
    }
}
