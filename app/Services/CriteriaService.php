<?php

namespace App\Services;

use App\Models\Criteria;

class CriteriaService
{
    /**
     * Получает критерии по массиву идентификаторов
     * 
     * @param  array
     * @return Collection
     */
    public function getCriteriasByIds(array $ids)
    {
        $criterias = Criteria::whereIn('id', $ids)->get();
        return $criterias;
    }
}