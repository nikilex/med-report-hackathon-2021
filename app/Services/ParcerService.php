<?php

namespace App\Services;

use DiDom\Document;

class ParcerService
{
    /**
     * @var DiagnosisService
     */
    protected DiagnosisService $diagnosisService;

    public function __construct(
        DiagnosisService $diagnosisService
    ) {
        $this->diagnosisService = $diagnosisService;
    }

    public function parceHtml($html)
    {
        $document = new Document();

        // $document->loadHtml($html);
        $document->loadHtmlFile($html);
        $diagnoses    = $document->find('.R25C1');
        $examinations = $document->find('.R29C1');

        $returnArr = array_merge($this->formatedDiagnoses($diagnoses), $this->formatedExaminations($examinations));

        return $returnArr;
    }

    public function formatedDiagnoses(array $diagnoses)
    {
        $resultAppend = [];

        foreach ($diagnoses as $item) {
            $item = $item->text();
            $diagKey = 'dd';
            $diag = '';

            $exp = explode(' ', $item);

            foreach ($exp as $key => $value) {
                if ($key == 0) {
                    $diagKey = $value;
                } else {
                    if (trim($value) != '') {
                        $diag .= $value . ' ';
                    }
                }
            }
            $resultAppend = [
                'diagnoses' => [
                    'code' => $diagKey,
                    'text' => substr($diag, 0, -1),
                ]
            ];
            // array_push($resultArr, $resultAppend);
        }

        return $resultAppend;
    }

    public function formatedExaminations($document)
    {
        $resultArr = [
            'examination' => [],
            'treatment' => []
        ];
        $point = false;

        if (count($document) > 0) {
            $examinations = explode(PHP_EOL, $document[0]->text());

            foreach ($examinations as $exam) {
                $append = trim($exam);

                if ($append == 'Лечение:') {
                    $point = true;
                }

                if ($append != '' && $append != 'Лечение:' && $append != 'Обследование:') {
                    $resultPoint = $point ? 'treatment' : 'examination';
                    array_push($resultArr[$resultPoint], $append);
                }
            }
        }

        return $resultArr;
    }

    /**
     * Проверяет карточку на совпадение
     * назначеных обследований
     * на вход должен быть массив результат
     * выполнения метода $this->parceHtml()
     * @param  array $card 
     */
    public function checkCard(array $card)
    {
        // $str1 = 'ПЦР мазок из зева к микоплазме пневмо и хламидии пневмо';
        // $str2 = 'мазок из зева к микоплазме пневмо и хламидии пневмо';
        // dd(similar_text($str1, $str2, $perc), $perc);
        // treatment
        // $resultArr = [
        //     'diagnoses'   => [],
        //     'examination' => [
        //         'value' => '',
        //         'color' => '',
        //         'type'  => ''
        //     ]
        // ];


        $diagnosisInBd = $this->diagnosisService->getByCodeWithExaminations($card['diagnoses']['code']);

        $resultArr = [
            'diagnoses' => [
                'code' => $diagnosisInBd->code,
                'text' => $diagnosisInBd->name
            ],
            'examinations' => []
        ];

        // $examinationCountInDB = $diagnosisInBd->examinations->where('type', 'examination')->count();
        // $treatmentCountInDB   = $diagnosisInBd->examinations->where('type', 'treatment')->count();

        $this->checkCardWithoutBeforeText('examination', $card, $diagnosisInBd, $resultArr);
        $this->checkCardWithoutBeforeText('treatment', $card, $diagnosisInBd, $resultArr);

        // if( count($card['examination']) > $examinationCountInDB) {
        //     $this->eachExaminationForCardItems('examination', $card, $diagnosisInBd, $resultArr);
        // } else {
        //     $this->eachExaminations('examination', $card, $diagnosisInBd, $resultArr);
        // }

        // if( count($card['treatment']) > $treatmentCountInDB) {
        //     $this->eachExaminationForCardItems('treatment', $card, $diagnosisInBd, $resultArr);
        // } else {
        //     $this->eachExaminations('treatment', $card, $diagnosisInBd, $resultArr);
        // }

        // if( count($card['examination']) + count($card['treatment']) > $diagnosisInBd->examinations->count()) {
        //     $this->eachExaminationForCardItems('examination', $card, $diagnosisInBd, $resultArr);
        //     $this->eachExaminationForCardItems('treatment', $card, $diagnosisInBd, $resultArr);

        // } else {
        //     $this->eachExaminations('examination', $card, $diagnosisInBd, $resultArr);
        //     $this->eachExaminations('treatment', $card, $diagnosisInBd, $resultArr);
        // }
        return $resultArr;
    }

    // public function eachExaminationForCardItems($type, &$card, &$diagnosisInBd, &$resultArr)
    // {
    //     foreach ($card[$type] as $key => $examinationParce) {

    //         $color   = 'blue';
    //         $message = 'Доп.';
    //         foreach ($diagnosisInBd->examinations as $examinationModel) {
    //             similar_text($examinationParce, $examinationModel->name, $perc);

    //             if ($perc > 50) {
    //                 $color = 'green';
    //                 $message = 'В порядке';
    //                 break;
    //             } else {
    //                 $color = 'blue';
    //             }
    //         }

    //         $pushArr = [
    //             'value'   => $card[$type][$key],
    //             'color'   => $color,
    //             'message' => $message,
    //             'type'    => $type
    //         ];

    //         array_push($resultArr['examinations'], $pushArr);
    //     }
    // }

    // public function eachExaminations($type, &$card, &$diagnosisInBd, &$resultArr)
    // {
    //     $meta = 0;
    //     foreach ($diagnosisInBd->examinations as $examinationModel) {

    //         $color = 'red';
    //         $message = 'Отсутствует';

    //         if ($examinationModel->type == $type) {
    //             foreach ($card[$type] as $key => $examinationParce) {
    //                 similar_text($examinationParce, $examinationModel->name, $perc);
    //                 if ($perc > 50) {
    //                     $color = 'green';
    //                     $message = 'В порядке';
    //                     break;
    //                 } else {
    //                     if ($meta < count($card[$type])) {
    //                         $pushArr = [
    //                             'value'   => $examinationParce,
    //                             'color'   => 'blue',
    //                             'message' => 'Доп.',
    //                             'type'    => $type
    //                         ];

    //                         array_push($resultArr['examinations'], $pushArr);

    //                         $meta++;
    //                     }

    //                     // $color = 'red';
    //                 }
    //             }

    //             $pushArr = [
    //                 'value'   => $examinationModel->name,
    //                 'color'   => $color,
    //                 'message' => $message,
    //                 'type'    => $type
    //             ];

    //             array_push($resultArr['examinations'], $pushArr);
    //         }
    //     }
    // }

    public function checkCardWithoutBeforeText($type, &$card, &$diagnosisInBd, &$resultArr)
    {
        foreach ($diagnosisInBd->examinations as $examinationModel) {

            $color = 'red';
            $message = 'Отсутствует';

            if ($examinationModel->type == $type && $examinationModel->examinations_similar_id == null) {
                foreach ($card[$type] as $key => $examinationParce) {

                    similar_text($examinationParce, $examinationModel->name, $perc);

                    if ($perc > 70) {
                        $color = 'green';
                        $message = 'В порядке';
                        break;
                    } else {
                        
                        foreach($examinationModel->similarExam as $similarExam) {
                            
                            similar_text($examinationParce, $similarExam->name, $percSimilar);
                            
                            if ( $percSimilar > 70) {
                                $color = 'green';
                                $message = 'В порядке';
                                break;
                            } else {
                                
                            }
                        }
                       // $color = 'red';
                    }
                }

                if ($color == 'red') {
                    $pushArr = [
                        'value'   => $examinationModel->name,
                        'color'   => $color,
                        'message' => $message,
                        'type'    => $type
                    ];

                    array_push($resultArr['examinations'], $pushArr);
                }
            }
        }
        
        // $tmpCollect = collect($resultArr['examinations']);

        // foreach ($card[$type] as $cardValue) {
            
        //     if($tmpCollect->where('value', $cardValue)->isEmpty()) {
        //         $pushArr = [
        //             'value'   => $cardValue,
        //             'color'   => 'blue',
        //             'message' => 'Доп.',
        //             'type'    => $type
        //         ];

        //         array_push($resultArr['examinations'], $pushArr);
        //     }
        // }
    }

    public function check($type, &$card, &$diagnosisInBd, &$resultArr)
    {

        foreach ($diagnosisInBd->examinations as $examinationModel) {

            $color = 'red';
            $message = 'Отсутствует';

            if ($examinationModel->type == $type) {
                foreach ($card[$type] as $key => $examinationParce) {
                    similar_text($examinationParce, $examinationModel->name, $perc);
                    if ($perc > 50) {
                        $color = 'green';
                        $message = 'В порядке';
                        break;
                    } else {
                        $color = 'red';
                    }
                }

                $pushArr = [
                    'value'   => $examinationModel->name,
                    'color'   => $color,
                    'message' => $message,
                    'type'    => $type
                ];

                array_push($resultArr['examinations'], $pushArr);
            }
        }
        
        $tmpCollect = collect($resultArr['examinations']);

        foreach ($card[$type] as $cardValue) {
            
            if($tmpCollect->where('value', $cardValue)->isEmpty()) {
                $pushArr = [
                    'value'   => $cardValue,
                    'color'   => 'blue',
                    'message' => 'Доп.',
                    'type'    => $type
                ];

                array_push($resultArr['examinations'], $pushArr);
            }
        }
    }

    /**
     * Парсит обследования или лечения
     * 
     * @param  string
     * @return array
     */
    public function parseRequestExam(string $examinations)
    {
        $result = explode(',', $examinations);
        return $result;
    }

    /**
     * Распарсивает массив в строку
     */
}
