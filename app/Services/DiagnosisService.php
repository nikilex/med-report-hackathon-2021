<?php

namespace App\Services;

use App\Models\Diagnosis;

class DiagnosisService
{
    /**
     * Получает запись по идентификатору
     * 
     * @param int $id
     * 
     * @return Diagnosis|null
     */
    public function getById(int $id)
    {
        return Diagnosis::find($id);
    }

    /**
     * Получает диагноз вместе с назначениями по идентификатору
     * @param  string $id
     * @return App\Models\Diagnosis
     */
    public function getByIdWithExaminations(string $id)
    {
        return Diagnosis::where('id', $id)->with('examinations')->first();
    }

    /**
     * Получает диагноз вместе с назначениями по идентификатору
     * без similar
     * @param  string $id
     * @return App\Models\Diagnosis
     */
    public function getByIdWithExaminationsWithoutSimilar(string $id)
    {
        return Diagnosis::where('id', $id)->with('examinations', function($q) {
            $q->where('examinations_similar_id', NULL);
        })->first();
    }

    /**
     * Получает диагноз вместе с назначениями
     * @param  string $code
     * @return App\Models\Diagnosis
     */
    public function getByCodeWithExaminations(string $code)
    {
        return Diagnosis::where('code', $code)->with(['examinations.similarExam'])->first();
    }

    /**
     * Получает все диагнозы
     */
    public function getAllDiagnoses()
    {
        return Diagnosis::get();
    }

    /**
     * Получает все диагнозы
     * 
     * @param int $perPage
     * 
     * @return Diagnosis
     */
    public function getAllPaginate($perPage = 15)
    {
        return Diagnosis::paginate($perPage);
    }

    /**
     * Получает критерии качества диагноза из
     * Приказа Минздрава России от 10.05.2017 N 203н
     * 
     * @param  int $id
     * @return Model
     */
    public function getDiagnosCriteria(int $id)
    {
        return Diagnosis::where('id', $id)->first()->criteria()->get();
    }

    /**
     * Получает диагноз вместе с критериями качества из
     * Приказа Минздрава России от 10.05.2017 N 203н
     * 
     * @param  int $id
     * @return Model
     */
    public function getDiagnosWithCriteria(int $id)
    {
        return Diagnosis::with('criteria')->where('id', $id)->first();
    }

    /**
     * Добавляет диагноз
     * 
     * @param array $data
     * 
     * @return 
     */
    public function store(array $data)
    {
        $diagnos = Diagnosis::create([
            'code' => $data['code'],
            'name' => $data['diagnos']
        ]);

        return $diagnos;
    }

    /**
     * Обновляет диагноз
     * 
     * @param array $data
     * 
     * @return 
     */
    public function update(array $data)
    {
        $diagnos = Diagnosis::where('id', $data['diagnosId'])
        ->update([
            'code' => $data['code'],
            'name' => $data['diagnos']
        ]);

        return $diagnos;
    }

    /**
     * Добавляет диагноз
     * 
     * @param array $data
     * 
     * @return 
     */
    public function delete(array $data)
    {
        $diagnos = Diagnosis::find($data['diagnosId'])
        ->delete();

        return $diagnos;
    }
    
}