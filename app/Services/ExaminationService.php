<?php

namespace App\Services;

use App\Models\Examination;

class ExaminationService
{
    public function getById(int $id)
    {
        return Examination::find($id);
    }

    public function getByName(string $name)
    {
        return Examination::where('name', $name)->first();
    }

    public function getByDiagnosIdWithoutSimilar(int $id, $type = '', $perPage = 10)
    {
        return Examination::where('diagnosis_id', $id)
            ->where('type', 'like', '%' . $type . '%')
            ->where('examinations_similar_id', NULL)
            ->paginate($perPage);
    }

    public function getSimilar(int $id, $perPage = 10)
    {
        return $this->getById($id)->similarExam()->paginate($perPage);
    }

    public function store(array $data)
    {
        $examination = Examination::create([
            'diagnosis_id' => $data['diagnosId'],
            'name'         => $data['exam_name'],
            'type'         => $data['examinationType']
        ]);

        $this->storeSimilar($examination->id, $data);

        return $examination;
    }

    public function storeSimilar(int $examId, array $data)
    {
        foreach ($data['similar'] as $similar) {
            if ($similar) {
                Examination::create([
                    'diagnosis_id'            => $data['diagnosId'],
                    'name'                    => $similar,
                    'type'                    => $data['examinationType'],
                    'examinations_similar_id' => $examId
                ]);
            }
        }

        return true;
    }

    public function update(int $id, array $data)
    {
        $examination = Examination::where('id', $id)->update([
            'name' => $data['exam_name'],
        ]);

        $this->updateSimilar($id, $data);

        return $examination;
    }

    public function updateSimilar(int $examId, array $data)
    {
        foreach ($data['similar'] as $key => $similar) {
            if (isset($data['similarId'][$key])) {
                $similarInBd = $this->getById($data['similarId'][$key]);
                if ($similar && $similarInBd != null) {
                    $similarInBd->update([
                        'name' => $similar
                    ]);
                }
            } else {
                if ($similar) {
                    Examination::create([
                        'diagnosis_id'            => $data['diagnosId'],
                        'name'                    => $similar,
                        'type'                    => $data['examinationType'],
                        'examinations_similar_id' => $examId
                    ]);
                }
            }
        }

        return true;
    }
}
