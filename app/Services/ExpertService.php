<?php

namespace App\Services;

use App\Models\Diagnosis;
use App\Models\Examination;
use App\Models\Expert;

class ExpertService
{
    protected ParcerService $parserService;

    public function __construct(
        ParcerService $parserService
    ) {
        $this->parcerService = $parserService;
    }

    /**
     * Получает все экспертизы
     */
    public function getAll()
    {
        return Expert::get();
    }

    public function getPaginate($perPage = 10)
    {
        return Expert::paginate($perPage);
    }

    /**
     * Производит экспертизу карточки пациента
     * 
     * @param  array
     * @return mixed
     */
    public function expert(array $data)
    {

        $diadnos = Diagnosis::where('id', $data['diagnos'])->with(['criteria', 'examinations'])->first();

        if (!$diadnos) {
            $diadnos = Diagnosis::where('code', $data['diagnos'])->with(['criteria', 'examinations'])->first();
        }

        $treatmentParce   = $this->parcerService->parseRequestExam($data['treatment']);

        $expert = [
            'criteria'     => [],
            'examinations' => [],
            'treatment'    => []
        ];

        $checkCriteria = 0;
        if (isset($data['criteria'])) {
            if(is_array($data['criteria'])) {
                foreach ($diadnos->criteria as $criteria) {
                    if (!in_array($criteria->id, $data['criteria'])) {
                        array_push($expert['criteria'], $criteria->id);
                    }
                }
            } else {
                $criteriaRequest = explode(',', $data['criteria']);
                foreach ($diadnos->criteria as $criteria) {

                    foreach($criteriaRequest as $criteriaValue) {
                        similar_text(trim($criteriaValue), $criteria->name, $perc);
                        if ($perc > 70) {
                            $checkCriteria = 1;
                            break;
                        }
                    }
                    if($checkCriteria != 1) {
                        array_push($expert['criteria'], $criteria->id);
                        $checkCriteria = 0;
                    }
                }
            }
        } else {
            $expert['criteria'] = $diadnos->criteria->pluck('id');
        }

        $check = 0;
        $checkTreatment = 0;
        foreach ($diadnos->examinations as $keyD => $examination) {

            if (isset($data['examination'])) {

                $examinationParse = $this->parcerService->parseRequestExam($data['examination']);
                $treatmentParse = $this->parcerService->parseRequestExam($data['treatment']);

               // dd($examinationParse);
                if ($examination->type == 'examination') {
                    foreach ($examinationParse as $key => $examText) {
                        similar_text( trim(preg_replace('/\s+/', ' ', $examText)) , $examination->name, $perc);
                        foreach($examination->similarExam()->get() as $examinationSimilar) {
                            similar_text( trim(preg_replace('/\s+/', ' ', $examinationSimilar)) , $examination->name, $perc);
                        }
                        // if($key == 3) {
                        //     dd($perc);
                        // }
                        if ($perc > 70) {
                            $check = 1;
                            break;
                        } else {
                            $check = 0;
                        }
                    }
                    if ($check != 1) {
                        
                        array_push($expert['examinations'], $examination->name);
                        $check == 0;
                    }
                    
                    
                }

                if ($examination->type == 'treatment') {
                    foreach ($treatmentParse as $examText) {
                        similar_text(trim(preg_replace('/\s+/', ' ', $examText)), $examination->name, $perc);
                        foreach($examination->similarExam()->get() as $examinationSimilar) {
                            similar_text( trim(preg_replace('/\s+/', ' ', $examinationSimilar)) , $examination->name, $perc);
                        }
                        if ($perc > 70) {
                            $checkTreatment = 1;
                            break;
                        } else {
                            $checkTreatment = 0;
                        }
                    }
                    if ($checkTreatment != 1) {
                        array_push($expert['treatment'], $examination->name);
                        $checkTreatment == 0;
                    }
                }
                
            }
        }


        // dd($expert['criteria']);

        return $expert;
    }

    private function removeSpecialChar($string) {
        $string = str_replace('', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * Добавляет историю экспертиз
     * 
     * @param  array
     * @return Collection
     */
    public function store(array $data)
    {
        $newExpert = Expert::create([
            'count_error_criteria'    => count($data['criteria']),
            'count_error_examination' => count($data['examinations']),
            'count_error_treatment'   => count($data['treatment']),
            'error_criteria'          => implode(',',$data['criteria']),
            'error_examination'       => implode(',',$data['examinations']),
            'error_treatment'         => implode(',',$data['treatment']),
        ]);

        return $newExpert;
    }
}
