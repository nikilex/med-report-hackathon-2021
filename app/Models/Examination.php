<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function similarExam()
    {
        return $this->hasMany(Examination::class, 'examinations_similar_id', 'id');
    }
}
