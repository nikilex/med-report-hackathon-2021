<h4>Реализованная функциональность</h4>
...
555
555
<ul>
	<li>Создан лендинг;</li>
	<li>Создана система авторизации;</li>
	<li>Разработан интерфейс системы;</li>
    <li>Парсер карточки пациента;</li>
    <li>Проведение экспертизы по загруженной карточке;</li>
    <li>Выгрузка печатного в формате PDF протокола после проведения экспертизы;</li>
	<li>Ведение журнала проведённых экспертих;</li>
	<li>Раздел аналитики в котором отображены все результаты проведения экспертих, а также график в разрезе ЛПУ;</li>
	<li>Проведение экспертизы по загруженной карточке;</li>
	<li>API метод, который принимает параметры карточки пациента, проводит экспертизы и возвращает результат экспертизы в формате JSON;</li>
</ul> 
<h4>Особенность проекта в следующем:</h4>
<ul>
 <li>Возможность провести экспертизу автоматически;</li>
 <li>Возможность посмотреть аналитику и на её основе принимать управленческие решения;</li>
 <li>Возможность взаимодействия с сервисом посредвом API;</li>  
 <li>Выгрузка печатного протокола;</li>
 <li>Возможность во время приёма выводить подсказки для назначений правильного лечения;</li>  
 </ul>
<h4>Основной стек технологий:</h4>
<ul>
    <li>LAMP/LEMP.</li>
	<li>HTML, CSS, JavaScript.</li>
	<li>PHP 7.4, MySQL.</li>
	<li>Laravel.</li>
	<li>Git.</li>
	<li>Gitlab.</li>
  
 </ul>
<h4>Демо</h4>
<p>Демо сервиса доступно по адресу: http://med-report.ru </p>
<p>Реквизиты тестового пользователя: email: <b>admin@admin.ru</b>, пароль: <b>password</b></p>


СРЕДА ЗАПУСКА
------------
1) развертывание сервиса производится на debian-like linux (debian 9+);
2) требуется установленный web-сервер с поддержкой PHP(версия 7.4+) интерпретации (apache, nginx);
3) требуется установленная СУБД MariaDB (версия 10+);
4) требуется установка composer;


УСТАНОВКА
------------
### Установка пакетов, клонирование проекта и настройка конфигурационных файлов

Выполните 
~~~
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install mariadb-client mariadb-server
git clone https://gitlab.com/nikilex/med-report-hackathon-2021.git
cd med-report-hackathon-2021
cp .env.example .env
~~~

### Установка зависимостей проекта

Установка зависимостей осуществляется с помощью [Composer](http://getcomposer.org/). Если у вас его нет вы можете установить его по инструкции
на [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

После этого выполнить команду в директории проекта:

~~~
composer install
~~~
### База данных

Необходимо создать пустую базу данных, а подключение к базе прописать в конфигурационный файл сервиса по адресу: med-report-hackathon-2021/.env

~~~
sudo systemctl restart mariadb
sudo mysql_secure_installation
mysql -u root -p
mypassword
CREATE DATABASE mynewdb;
quit
~~~

Файл .env в корневой папке проекта
~~~
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE= имя базы данных
DB_USERNAME= пользователь базы данных
DB_PASSWORD= пароль базы данных
~~~
### Выполнение миграций

Для заполнения базы данных таблицами и системной информацией выполните в корневой папке сервиса: 

~~~
php artisan migrate --seed
~~~

### Конфигурация apache

При использовании apache необходимо в корневую папку проекта поместить файл .htaccess с содержимым:

~~~
RewriteEngine on
RewriteCond %{REQUEST_URI} !^public
RewriteRule ^(.*)$ public/$1 [L]
~~~


Также необходимо по пути /папка_проекта/public поместить файл .htaccess с содержимым:

~~~
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    RewriteEngine On

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$
    RewriteRule ^ %1 [L,R=301]

    # Send Requests To Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ index.php [L]
</IfModule>
~~~


РАЗРАБОТЧИК

<h4>Алексей Никитин fullstack https://t.me/nikilex </h4>


