<?php

namespace Database\Seeders;

use App\Models\Diagnosis;
use Illuminate\Database\Seeder;

class DiagnosesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Diagnosis::create([
            'code' => 'J01',
            'name' => 'Острый синусит'
        ]);
        
        Diagnosis::create([
            'code' => 'I21.0',
            'name' => 'Нестабильная стенокардия'
        ]);
    }
}
